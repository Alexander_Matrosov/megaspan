<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php"); ?>
    <div class="container"^>
        <div class="contacts">
            <div class="breadcrumbs"><a href="#">Главная страница</a><a href="#">Мегаспан ГЕО 100</a></div>
            <h1 class="title">Контакты</h1>
            <div class="contacts-main">
                <div class="contacts-info">
                    <div class="contacts-info__box"><span>Адрес:</span>
                        <p>656023, РФ, Алтайский край, город Барнаул, ул. Заводской 9−ый проезд 1−Г</p>
                    </div>
                    <div class="contacts-info__box"><span>Мы работаем:</span>
                        <p>Понедельник - пятница, с 8:00 до 18:00</p>
                    </div>
                    <div class="contacts-info__box"><span>Телефон:</span>
                        <p>+7 3852) 338-000, 50-30-77 Региональные продажи;<br>+7 902 143-18-88 Виталий</p>
                    </div>
                    <div class="contacts-info__box"><span>Электронная почта:</span>
                        <p><a href="#">zakaz@fuleren.ru </a><span>- вопросы по вашему заказу, ассортименту, цене и т.д.</span></p>
                        <p><a href="#">partner@fuleren.ru </a><span>- по сотрудничеству для оптовиков</span></p>
                        <p><a href="#">director@fuleren.ru </a><span>- качество обслуживания клиента</span></p>
                    </div>
                </div>
                <div class="contacts-map"><img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/contacts-map.jpg" alt=""></div>
            </div>
        </div>
    </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>