<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");
define("HIDE_SIDEBAR", true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Страница не найдена");?>

    <div class="container">
        <div class="error-page">
            <div class="error-404">
                <p>4</p>
                <span>
                    <svg width="152" height="174" viewBox="0 0 152 174" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M6.15579 46.6754L76 6.35085L145.844 46.6754V127.325L76 167.649L6.15579 127.325L6.15579 46.6754Z" stroke="#448638" stroke-width="11"/>
                    </svg>
                </span>
                <p>4</p>
            </div>
            <p class="not-found">Страница не найдена</p>
            <a class="link-page" href="/">Вернуться на главную</a>
        </div>
    </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>