<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("");
?>
<? if($USER->IsAuthorized()): ?>
    <?$APPLICATION->IncludeComponent(
        "bitrix:main.include",
        "",
        Array(
            "AREA_FILE_SHOW" => "file",
            "AREA_FILE_SUFFIX" => "inc",
            "EDIT_TEMPLATE" => "",
            "PATH" => "/include/personal/header.php"
        )
    );?>
    <?
    $filter = array("ID" => $USER->GetID());
    $rsUser = CUser::GetList(($by = "personal_country"), ($order = "desc"), $filter); // выбираем пользователей
    $arUser = $rsUser->Fetch();
    ?>
    <span class="personal__user"><?= $arUser['EMAIL'] ?></span>
    <form id="profile" method="post" action="/local/script/user.php">
        <div class="user-data">
            <div class="input"><input type="text" value="<?= $arUser['EMAIL'] ?>" name="email" required><span>Email</span></div>
            <div class="input"><input type="text" value="<?= $arUser['LOGIN'] ?>" name="login" required><span>Логин</span></div>
            <div class="input"><input type="password" name="password" required><span>Пароль</span></div>
            <div class="input"><input type="password" name="confirm_password" required><span>Подтверждение пароля</span></div>
            <div class="input"><input type="text" name="fio" required><span>Ф.И.О.</span></div>
            <div class="input"><input type="text" name="company" required><span>Компания</span></div>
            <div class="input"><input type="text" name="inn" required><span>ИНН</span></div>
            <div class="input"><input type="tel" name="phone" required><span>Телефон</span></div>
        </div>
        <div class="user-data">
            <span class="user-data__title">Адрес</span>
            <div class="user-inputs">
                <div class="input"><input type="text" name="city" required><span>Город</span></div>
                <div class="input"><input type="text" name="street" required><span>Улица</span></div>
                <div class="input"><input type="text" name="home" required><span>Дом</span></div>
                <div class="input"><input type="text" name="office" required><span>Офис/Склад</span></div>
                <div class="textarea"><textarea name="comment"></textarea><span>Комментарий</span></div>
            </div>
        </div>
        <div class="user-checkboxes">
            <span class="user-data__title">Уведомления</span>
            <label class="checkbox"><input type="checkbox" name="checkbox1"><span></span><p>Высылать счет после заявки</p></label>
            <label class="checkbox"><input type="checkbox" name="checkbox2"><span></span><p>Озвучивать мне сумму по телефону</p></label>
            <label class="checkbox"><input type="checkbox" name="checkbox3"><span></span><p>Хочу получать уведомление об отгрузке на телефон</p></label>
            <label class="checkbox"><input type="checkbox" name="checkbox4"><span></span><p>Если отгрузка меняется по сравнению с выставленным счётом прошу мне звонить для уточнения изменения в заказе</p></label>
            <label class="checkbox"><input type="checkbox" name="checkbox5"><span></span><p>Всегда уведомлять меня о новых товарах письмом на почту</p></label>
        </div>
        <a class="btn-green" href="#">Сохранить</a>
        <button>df</button>
        <input type="hidden" name="user-id" value="<?= $arUser['ID'] ?>">
        <input type="hidden" name="send" value="profile">
    </form>
    <?$APPLICATION->IncludeComponent(
        "bitrix:main.include",
        "",
        Array(
            "AREA_FILE_SHOW" => "file",
            "AREA_FILE_SUFFIX" => "inc",
            "EDIT_TEMPLATE" => "",
            "PATH" => "/include/personal/bottom.php"
        )
    );?>
<? else: ?>
    <? header('Location: /'); ?>
<? endif; ?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>