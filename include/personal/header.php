<div class="container">
    <div class="personal">
        <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "main.breadcrumb", Array(
            "PATH" => "",	// Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
            "SITE_ID" => "s1",	// Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
            "START_FROM" => "0",	// Номер пункта, начиная с которого будет построена навигационная цепочка
        ),
            false
        );?>
        <h1 class="title">Личный кабинет</h1>
        <div class="personal-main">
            <div class="personal-nav">
                <? $APPLICATION->IncludeComponent("bitrix:menu", "personal.menu", array(
                    "ALLOW_MULTI_SELECT" => "N",    // Разрешить несколько активных пунктов одновременно
                    "CHILD_MENU_TYPE" => "left",    // Тип меню для остальных уровней
                    "DELAY" => "N",    // Откладывать выполнение шаблона меню
                    "MAX_LEVEL" => "1",    // Уровень вложенности меню
                    "MENU_CACHE_GET_VARS" => array(    // Значимые переменные запроса
                        0 => "",
                    ),
                    "MENU_CACHE_TIME" => "3600",    // Время кеширования (сек.)
                    "MENU_CACHE_TYPE" => "N",    // Тип кеширования
                    "MENU_CACHE_USE_GROUPS" => "N",    // Учитывать права доступа
                    "ROOT_MENU_TYPE" => "personal",    // Тип меню для первого уровня
                    "USE_EXT" => "N",    // Подключать файлы с именами вида .тип_меню.menu_ext.php
                ),
                    false
                ); ?>
                <a class="exit-btn" href="?logout=yes">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M4.99805 3H18.998C20.101 3 20.998 3.897 20.998 5V9H18.998V5H4.99805V19H18.998V15H20.998V19C20.998 20.103 20.101 21 18.998 21H4.99805C3.89505 21 2.99805 20.103 2.99805 19V5C2.99805 3.897 3.89605 3 4.99805 3Z"
                            fill="black"/>
                        <path d="M13 16L8 12L13 8V11.001H21V13.001H13V16Z" fill="black"/>
                    </svg>
                    <span>Выход</span>
                </a>
            </div>
            <div class="personal-block">