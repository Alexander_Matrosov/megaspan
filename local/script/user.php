<?
define("NO_KEEP_STATISTIC", true);
define("NO_AGENT_CHECK", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
global $USER;


// Авторизация по Email или Логину
if(isset($_POST['send']) && $_POST['send'] == 'singin'){
    if(filter_var($_POST['login'], FILTER_VALIDATE_EMAIL)){

        $cUser = new CUser;
        $sort_by = "ID";
        $sort_ord = "ASC";
        $arFilter = array("EMAIL" => $_POST['login'], "ACTIVE" => 'Y',);

        $dbUsers = $cUser->GetList($sort_by, $sort_ord, $arFilter);
        while ($arUser = $dbUsers->Fetch())
            $user = array("ID" => $arUser['ID'], "LOGIN" => $arUser['LOGIN'], "EMAIL" => $arUser['EMAIL'], "PASSWORD" => $arUser['PASSWORD']);

        if($user){
            $salt = substr($user['PASSWORD'], 0, (strlen($user['PASSWORD']) - 32));
            $realPassword = substr($user['PASSWORD'], -32);
            $password = md5($salt.$_POST['password']);
            if($password == $realPassword){
                $USER ->Authorize($user['ID']);
                die(json_encode(true));
            }
        }
        die(json_encode(false));
    } else {

        if (!is_object($USER)) $USER = new CUser;
        $arAuthResult = $USER->Login($_POST['login'], $_POST['password'], "Y");
        if ($arAuthResult['MESSAGE']) {
            die(json_encode(false));
        } else {
            die(json_encode(true));
        }
    }
}

// Регистрация
if(isset($_POST['send']) && $_POST['send'] == 'singup'){
    $arResult = $USER->Register($_POST['email'],'','',$_POST['password'],$_POST['password'], $_POST['email']);
    if($arResult['TYPE'] == 'OK'){
        die(json_encode(true));
    } else {
        die(json_encode(false));
    }
}

// Редактирование профиля
if(isset($_POST['send']) && $_POST['send'] == 'profile'){

    $fields = array(
        "NAME" => "Сергей",
        "LAST_NAME" => "Иванов",
        //"EMAIL" => $_POST['email'],
        "LOGIN" => $_POST['login'],
        "PASSWORD" => $_POST['password'],
        "CONFIRM_PASSWORD" => $_POST['confirm_password'],
    );

    $USER->Update($_POST['user-id'], $fields);
    $strError .= $USER->LAST_ERROR;
}
?>