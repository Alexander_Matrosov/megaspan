<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Page\Asset;
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title><? $APPLICATION->ShowTitle() ?></title>

    <meta name="theme-color" content="#c9e0e04d">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="shortcut icon" href="<?= SITE_TEMPLATE_PATH ?>/assets/img/favicons/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href=<?= SITE_TEMPLATE_PATH ?>/assets/img/favicons/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="57x57" href=<?= SITE_TEMPLATE_PATH ?>/assets/img/favicons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href=<?= SITE_TEMPLATE_PATH ?>/assets/img/favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href=<?= SITE_TEMPLATE_PATH ?>/assets/img/favicons/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href=<?= SITE_TEMPLATE_PATH ?>/assets/img/favicons/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href=<?= SITE_TEMPLATE_PATH ?>/assets/img/favicons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href=<?= SITE_TEMPLATE_PATH ?>/assets/img/favicons/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href=<?= SITE_TEMPLATE_PATH ?>/assets/img/favicons/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= SITE_TEMPLATE_PATH ?>/assets/img/favicons/apple-touch-icon-180x180.png">

    <?php
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/assets/styles/main.min.css");

    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/main.min.js");
    ?>
    <? $APPLICATION->ShowHead(); ?>
</head>
<body>
<div id="panel"><?$APPLICATION->ShowPanel()?></div>
<header class="header">
    <div class="container">
        <div class="header-main">
            <a class="header-logo" href="/"><img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/logo.png" alt=""></a>
            <div class="header-search">
                <span>Только опт</span>
                <input type="text" placeholder="Поиск по товарам">
                <div class="header-contacts"><span>+7(3852) 338-000; 50-30-77</span><span> Пн-Пт 8:00 - 18:00 (Барнаул)</span></div>
            </div>

            <a href="#">
                <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M5.00023 4.16669C2.20982 5.84102 0.416894 9.16669 10.0001 16.6667C19.1666 9.16669 17.4999 6.25002 15.8333 4.58336C13.8987 2.64885 11.2501 4.16669 10.0001 5.41669C8.75008 4.16669 7.08346 2.91669 5.00023 4.16669Z" stroke="white" stroke-width="2.5"/>
                </svg>
                <p>Мой ассортимент</p>
                <span>32</span>
            </a>
            <? if(CUser::IsAuthorized()): ?>
                <a href="/personal/">
                    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M3.33325 17.5C3.33314 15.4166 3.66659 11.6666 9.99992 11.6666C16.1849 11.6666 16.694 15.4166 16.694 17.5" stroke="white" stroke-width="2.5"/>
                        <circle cx="9.99992" cy="5.83329" r="2.91667" stroke="white" stroke-width="2.5"/>
                    </svg>
                    <p>Личный кабинет</p>
                </a>
            <? else: ?>
                <a href="javascript:void(0)" data-izimodal-open="#modal-personal" data-izimodal-transitionin="fadeInDown">
                    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M3.33325 17.5C3.33314 15.4166 3.66659 11.6666 9.99992 11.6666C16.1849 11.6666 16.694 15.4166 16.694 17.5" stroke="white" stroke-width="2.5"/>
                        <circle cx="9.99992" cy="5.83329" r="2.91667" stroke="white" stroke-width="2.5"/>
                    </svg>
                    <p>Авторизоваться</p>
                </a>
            <? endif; ?>
            <?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", "header.basket", Array(
                "HIDE_ON_BASKET_PAGES" => "Y",	// Не показывать на страницах корзины и оформления заказа
                "PATH_TO_AUTHORIZE" => "",	// Страница авторизации
                "PATH_TO_BASKET" => SITE_DIR."personal/cart/",	// Страница корзины
                "PATH_TO_ORDER" => SITE_DIR."personal/order/make/",	// Страница оформления заказа
                "PATH_TO_PERSONAL" => SITE_DIR."personal/",	// Страница персонального раздела
                "PATH_TO_PROFILE" => SITE_DIR."personal/",	// Страница профиля
                "PATH_TO_REGISTER" => SITE_DIR."login/",	// Страница регистрации
                "POSITION_FIXED" => "N",	// Отображать корзину поверх шаблона
                "SHOW_AUTHOR" => "Y",	// Добавить возможность авторизации
                "SHOW_EMPTY_VALUES" => "Y",	// Выводить нулевые значения в пустой корзине
                "SHOW_NUM_PRODUCTS" => "Y",	// Показывать количество товаров
                "SHOW_PERSONAL_LINK" => "Y",	// Отображать персональный раздел
                "SHOW_PRODUCTS" => "N",	// Показывать список товаров
                "SHOW_REGISTRATION" => "Y",	// Добавить возможность регистрации
                "SHOW_TOTAL_PRICE" => "Y",	// Показывать общую сумму по товарам
            ),
                false
            );?>
        </div>
        <nav class="nav">
            <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list", 
	"header.dropdown.menu", 
	array(
		"ADD_SECTIONS_CHAIN" => "N",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "N",
		"COUNT_ELEMENTS" => "Y",
		"COUNT_ELEMENTS_FILTER" => "CNT_ACTIVE",
		"FILTER_NAME" => "sectionsFilter",
		"IBLOCK_ID" => "6",
		"IBLOCK_TYPE" => "1c_catalog",
		"SECTION_CODE" => "",
		"SECTION_FIELDS" => array(
			0 => "ID",
			1 => "CODE",
			2 => "NAME",
			3 => "",
		),
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_URL" => "/#SECTION_CODE#/",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SHOW_PARENT_NAME" => "Y",
		"TOP_DEPTH" => "3",
		"VIEW_MODE" => "LIST",
		"COMPONENT_TEMPLATE" => "header.dropdown.menu"
	),
	false
);?>
            <?$APPLICATION->IncludeComponent("bitrix:menu", "top.menu", Array(
                "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                "DELAY" => "N",	// Откладывать выполнение шаблона меню
                "MAX_LEVEL" => "1",	// Уровень вложенности меню
                "MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
                    0 => "",
                ),
                "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                "MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
                "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
                "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
            ),
                false
            );?>
        </nav>
    </div>
</header>
<header class="header-mobile">
    <div class="header-left">
        <div class="burger-menu">
            <span class="burger-top"></span>
            <span class="burger-middle"></span>
            <span class="burger-bottom"></span></div>
        <div class="header-mobile__logo">
            <img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/mobile-logo.png" alt=""></div>
    </div>
    <div class="header-right">
        <div class="search-mobile">
            <a class="btn" href="#"><svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M11.1188 22.2365C13.5858 22.236 15.9817 21.4102 17.9249 19.8905L24.0347 26L26 24.0349L19.8902 17.9254C21.4107 15.982 22.2371 13.5857 22.2377 11.1182C22.2377 4.98792 17.2495 0 11.1188 0C4.98819 0 0 4.98792 0 11.1182C0 17.2486 4.98819 22.2365 11.1188 22.2365ZM11.1188 2.77956C15.7179 2.77956 19.458 6.51946 19.458 11.1182C19.458 15.717 15.7179 19.4569 11.1188 19.4569C6.51981 19.4569 2.77971 15.717 2.77971 11.1182C2.77971 6.51946 6.51981 2.77956 11.1188 2.77956Z" fill="white"/>
                </svg></a>
        </div>
        <div class="basket-mobile"><svg width="28" height="26" viewBox="0 0 28 26" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M10.457 25C11.0525 25 11.5352 24.5097 11.5352 23.9048C11.5352 23.2999 11.0525 22.8096 10.457 22.8096C9.8616 22.8096 9.37891 23.2999 9.37891 23.9048C9.37891 24.5097 9.8616 25 10.457 25Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M22.3164 25C22.9118 25 23.3945 24.5097 23.3945 23.9048C23.3945 23.2999 22.9118 22.8096 22.3164 22.8096C21.721 22.8096 21.2383 23.2999 21.2383 23.9048C21.2383 24.5097 21.721 25 22.3164 25Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M1.83203 2H6.14452L9.03388 16.6652C9.13247 17.1694 9.4025 17.6224 9.79671 17.9448C10.1909 18.2671 10.6842 18.4384 11.1901 18.4285H21.6695C22.1754 18.4384 22.6687 18.2671 23.0629 17.9448C23.4571 17.6224 23.7271 17.1694 23.8257 16.6652L25.5507 7.47618H7.22264" stroke="white" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/></svg></div>
        <div class="mobile-filter"><svg width="27" height="25" viewBox="0 0 27 25" fill="none" xmlns="http://www.w3.org/2000/svg"><line x1="2.21875" y1="12.5526" x2="25.2188" y2="12.5526" stroke="white" stroke-width="3" stroke-linecap="round"/><line x1="2.21875" y1="2.97363" x2="25.2188" y2="2.97363" stroke="white" stroke-width="3" stroke-linecap="round"/><circle class="circle-top" cx="19.7188" cy="3" r="3" fill="white"/><circle class="circle-middle" cx="7.77134" cy="12.4736" r="2.94736" fill="white"/><line x1="2.21875" y1="22.1316" x2="25.2188" y2="22.1316" stroke="white" stroke-width="3" stroke-linecap="round"/><circle class="circle-bottom" cx="12.7188" cy="22" r="3" fill="white"/></svg></div>
    </div>
    <div class="menu-mobile">
        <div class="menu-phone">
            <span>+7(3852) 338-000; 50-30-77</span>
            <span>Пн-Пт 8:00 - 18:00 (Барнаул)</span>
        </div>
        <div class="main-list">
            <?$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "main.menu.left", Array(
                "ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
                "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
                "CACHE_GROUPS" => "N",	// Учитывать права доступа
                "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                "CACHE_TYPE" => "N",	// Тип кеширования
                "COUNT_ELEMENTS" => "Y",	// Показывать количество элементов в разделе
                "COUNT_ELEMENTS_FILTER" => "CNT_ACTIVE",	// Показывать количество
                "FILTER_NAME" => "sectionsFilter",	// Имя массива со значениями фильтра разделов
                "IBLOCK_ID" => "4",	// Инфоблок
                "IBLOCK_TYPE" => "content",	// Тип инфоблока
                "SECTION_CODE" => $_REQUEST["SECTION_CODE"],	// Код раздела
                "SECTION_FIELDS" => array(	// Поля разделов
                    0 => "NAME",
                    1 => "",
                ),
                "SECTION_ID" => $_REQUEST["SECTION_ID"],	// ID раздела
                "SECTION_URL" => "#SECTION_CODE#/",	// URL, ведущий на страницу с содержимым раздела
                "SECTION_USER_FIELDS" => array(	// Свойства разделов
                    0 => "",
                    1 => "",
                ),
                "SHOW_PARENT_NAME" => "Y",	// Показывать название раздела
                "TOP_DEPTH" => "3",	// Максимальная отображаемая глубина разделов
                "VIEW_MODE" => "LIST",	// Вид списка подразделов
            ),
                false
            );?>
        </div>
    </div>
</header>
<div class="wrapper">
