<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if (empty($arResult)) {
    return '';
}

$res = '<div class="breadcrumbs">';

$elCount = count($arResult);
foreach ($arResult as $index => $item) {
    $link = (!empty($item['LINK']) && $index < ($elCount - 1)) ? $item['LINK'] : '';
    $title = $item['TITLE'];
    if ($index == $elCount - 1){
        $res .= '<span>' . $title . '</span>';
    } else {
        $res .= '<a href="' . $link . '">' . $title . '</a>';
    }
}
$res .= '</div>';
return $res;
