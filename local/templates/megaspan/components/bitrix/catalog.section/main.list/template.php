<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 *
 *  _________________________________________________________________________
 * |	Attention!
 * |	The following comments are for system use
 * |	and are required for the component to work correctly in ajax mode:
 * |	<!-- items-container -->
 * |	<!-- pagination-container -->
 * |	<!-- component-end -->
 */

$this->setFrameMode(true); ?>
<div class="catalog-main">
    <div class="breadcrumbs"><a href="#">Главная страница</a><a href="#">Мегаспан ГЕО 100</a></div>
    <h1 class="catalog__title">Мегаспан А, В, С, D, Геотекстиль</h1>
    <div class="sorting">
        <div class="sorting-values"><span>Сортировать: </span>
            <select class="sorting-values__drop">по возрастанию цены
                <option>по возрастанию цены</option>
                <option>по возрастанию цены</option>
                <option>по возрастанию цены</option>
            </select>
        </div>
        <div class="sorting-views">
            <a class="sorting-item sorting-row" href="#">
                <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect class="sorting-fill" x="5" y="5" width="15" height="3" fill="#CCCCCC"/>
                    <rect class="sorting-fill" x="5" y="11" width="15" height="3" fill="#CCCCCC"/>
                    <rect class="sorting-fill" x="5" y="17" width="15" height="3" fill="#CCCCCC"/>
                    <rect class="sorting-stroke" x="0.5" y="0.5" width="24" height="24" stroke="#CCCCCC"/>
                </svg>
            </a>
            <a class="sorting-item sorting-list" href="#">
                <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect class="sorting-fill" x="5" y="4" width="15" height="2" fill="#CCCCCC"/>
                    <rect class="sorting-fill" x="5" y="9" width="15" height="2" fill="#CCCCCC"/>
                    <rect class="sorting-fill" x="5" y="14" width="15" height="2" fill="#CCCCCC"/>
                    <rect class="sorting-fill" x="5" y="19" width="15" height="2" fill="#CCCCCC"/>
                    <rect class="sorting-stroke" x="0.5" y="0.5" width="24" height="24" stroke="#CCCCCC"/>
                </svg>
            </a>
            <a class="sorting-item sorting-card sorting-active" href="#">
                <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect class="sorting-stroke" x="0.5" y="0.5" width="24" height="24" stroke="#CCCCCC"/>
                    <rect class="sorting-fill" x="5" y="5" width="6" height="6" fill="#CCCCCC"/>
                    <rect class="sorting-fill" x="5" y="14" width="6" height="6" fill="#CCCCCC"/>
                    <rect class="sorting-fill" x="14" y="5" width="6" height="6" fill="#CCCCCC"/>
                    <rect class="sorting-fill" x="14" y="14" width="6" height="6" fill="#CCCCCC"/>
                </svg>
            </a>
        </div>
    </div>
    <div class="catalog-items">
        <div class="card-list">
            <? foreach($arResult['ITEMS'] as $arItems): ?>
                    <div class="card">
                        <a class="card-link" href="<?= $arItems['DETAIL_PAGE_URL'] ?>"></a>
                        <div class="card-img">
                            <div class="medium-image js-medium-size"><img src="<?= $arItems['PREVIEW_PICTURE']['SRC']  ?>" alt=""></div>
                            <div class="big-image js-big-size"><img src="<?= $arItems['PREVIEW_PICTURE']['SRC']  ?>" alt=""></div>
                        </div>
                        <div class="card-name">
                            <a class="stock stock-list" href="#">«Держи пять»</a>
                            <p><?= $arItems['NAME'] ?></p>
                            <div class="card-box">
                                <p>цена</p><span>м2</span><a class="stock stock-row" href="#">«Держи пять»</a>
                            </div>
                        </div>
                        <a class="in-favorite__row" href="#"><svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.00023 7.16667C4.20982 8.841 2.41689 12.1667 12.0001 19.6667C21.1666 12.1667 19.4999 9.25 17.8333 7.58334C15.8987 5.64883 13.2501 7.16667 12.0001 8.41667C10.7501 7.16667 9.08346 5.91667 7.00023 7.16667Z" stroke="#44A63D" stroke-width="2.5"/></svg></a>
                        <? foreach($arItems['PRICES'] as $priceCode => $arPrice): ?>
                            <div class="card-info">
                                <div class="card-box">
                                    <p>цена</p><span><?= $arResult['PRICES'][$priceCode]['TITLE'] ?></span>
                                </div>
                                <div class="card-price">
                                    <p> <span><?= $arPrice['VALUE'] ?></span><span>руб.</span></p>
                                    <p>по 5 шт.</p>
                                </div>
                            </div>
                        <? endforeach; ?>
                    <div class="card-footer">
                        <div class="card-count">
                            <span class="js-minus-btn">
                                <svg width="8" height="2" viewBox="0 0 8 2" fill="none" xmlns="http://www.w3.org/2000/svg"><rect width="8" height="2" fill="#CCCCCC"/></svg>
                            </span>
                            <input type="text" value="0">
                            <span class="js-plus-btn">
                                <svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M5 3V0H3V3H0V5H3V8H5V5H8V3H5Z" fill="#44A63D"/></svg>
                            </span>
                        </div>
                        <form action="<?= $arItems['BUY_URL'] ?>" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="<?echo $arParams["ACTION_VARIABLE"]?>" value="BUY">
                            <input type="hidden" name="<?echo $arParams["PRODUCT_ID_VARIABLE"]?>" value="<?echo $arResult["ID"]?>">
                            <input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?echo GetMessage("CATALOG_ADD")?>">
                        </form>
                        <a class="btn" href="<?= $arItems['BUY_URL']?>"><svg width="64" height="35" viewBox="0 0 64 35" fill="none" xmlns="http://www.w3.org/2000/svg"><mask id="path-1-inside-1" fill="white"><path fill-rule="evenodd" clip-rule="evenodd" d="M0 35V10L10 0H10.9714H32H64V25L54 35H51.2H0Z"/></mask><path class="btn-background" fill-rule="evenodd" clip-rule="evenodd" d="M0 35V10L10 0H10.9714H32H64V25L54 35H51.2H0Z" fill="white"/><path d="M0 10L-0.707107 9.29289L-1 9.58579V10H0ZM0 35H-1V36H0V35ZM10 0V-1H9.58579L9.29289 -0.707107L10 0ZM64 0H65V-1H64V0ZM64 25L64.7071 25.7071L65 25.4142V25H64ZM54 35V36H54.4142L54.7071 35.7071L54 35ZM-1 10V35H1V10H-1ZM9.29289 -0.707107L-0.707107 9.29289L0.707107 10.7071L10.7071 0.707107L9.29289 -0.707107ZM10.9714 -1H10V1H10.9714V-1ZM32 -1H10.9714V1H32V-1ZM64 -1H32V1H64V-1ZM65 25V0H63V25H65ZM54.7071 35.7071L64.7071 25.7071L63.2929 24.2929L53.2929 34.2929L54.7071 35.7071ZM51.2 36H54V34H51.2V36ZM0 36H51.2V34H0V36Z" fill="#44A63D" mask="url(#path-1-inside-1)"/><path class="btn-logo" d="M29.4939 24.9445C29.8714 24.9445 30.1775 24.6336 30.1775 24.25C30.1775 23.8665 29.8714 23.5556 29.4939 23.5556C29.1164 23.5556 28.8103 23.8665 28.8103 24.25C28.8103 24.6336 29.1164 24.9445 29.4939 24.9445Z" stroke="#44A63D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path class="btn-logo" d="M37.0134 24.9445C37.391 24.9445 37.697 24.6336 37.697 24.25C37.697 23.8665 37.391 23.5556 37.0134 23.5556C36.6359 23.5556 36.3298 23.8665 36.3298 24.25C36.3298 24.6336 36.6359 24.9445 37.0134 24.9445Z" stroke="#44A63D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path class="btn-logo" d="M24.0251 10.3611H26.7595L28.5916 19.6598C28.6541 19.9795 28.8253 20.2667 29.0752 20.4711C29.3252 20.6755 29.6379 20.7841 29.9587 20.7778H36.6033C36.9241 20.7841 37.2368 20.6755 37.4868 20.4711C37.7367 20.2667 37.9079 19.9795 37.9705 19.6598L39.0642 13.8334H27.4431" stroke="#44A63D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg></a>
                    </div>
                        <a class="in-favorite" href="#">
                            <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.00023 7.16667C4.20982 8.841 2.41689 12.1667 12.0001 19.6667C21.1666 12.1667 19.4999 9.25 17.8333 7.58334C15.8987 5.64883 13.2501 7.16667 12.0001 8.41667C10.7501 7.16667 9.08346 5.91667 7.00023 7.16667Z" stroke="#44A63D" stroke-width="2.5"/></svg>
                            <span>в «Мой ассортимент»</span>
                        </a>
                </div>
            <? endforeach; ?>
        </div>
        <div class="pagination">
            <a href="#">1</a>
            <a class="pagination__active" href="#">2</a>
            <a href="#">3</a>
            <a href="#">4</a>
            <a href="#">5</a>
            <a href="#">6</a>
            <a href="#">7</a>
            <a href="#">8</a>
            <a href="#">9</a>
            <a href="#">...</a>
            <a href="#">35</a>
        </div>
        <?=$arResult['NAV_STRING']?>
    </div>
</div>
