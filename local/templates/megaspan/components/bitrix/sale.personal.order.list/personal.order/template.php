<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<?

?>
<?$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "",
    Array(
        "AREA_FILE_SHOW" => "file",
        "AREA_FILE_SUFFIX" => "inc",
        "EDIT_TEMPLATE" => "",
        "PATH" => "/include/personal/header.php"
    )
);?>
<div class="orders">
    <div class="orders-head">
        <div>Номер заказа</div>
        <div>Дата</div>
        <div>Сумма</div>
        <div>Статус</div>
    </div>
    <div class="orders-blocks">
        <? foreach($arResult['ORDERS'] as $arOrder): ?>
            <div class="order-wrapper">
                <div class="orders-head">
                    <div>Номер заказа</div>
                    <div>Дата</div>
                    <div>Сумма</div>
                    <div>Статус</div>
                </div>
                <div class="order-block">
                    <div><?= $arOrder['ORDER']['ID']?></div>
                    <div><?= $arOrder['ORDER']['DATE_INSERT_FORMATED'] ?></div>
                    <div><?= $arOrder['ORDER']['FORMATED_PRICE'] ?></div>
                    <div><?= $arResult['INFO']['STATUS'][$arOrder['ORDER']['STATUS_ID']]['NAME'] ?></div>
                    <div class="dropdown-order">
                        <a class="dropdown-order__close drop-open" href="#">Свернуть</a>
                        <a class="dropdown-order__open" href="#">Подробнее</a>
                    </div>
                    <div class="reload-order">
                        <a href="?COPY_ORDER=Y&amp;ID=<?= $arOrder['ORDER']['ID'] ?>""><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M11.9991 5.00003V2.21003C11.9991 1.76003 11.4591 1.54003 11.1491 1.86003L7.34905 5.65003C7.14905 5.85003 7.14905 6.16003 7.34905 6.36003L11.1391 10.15C11.4591 10.46 11.9991 10.24 11.9991 9.79003V7.00003C15.7291 7.00003 18.6791 10.42 17.8591 14.29C17.3891 16.56 15.5491 18.39 13.2891 18.86C9.71905 19.61 6.53905 17.16 6.05905 13.85C6.0234 13.615 5.90518 13.4003 5.72558 13.2446C5.54599 13.0888 5.31678 13.0021 5.07905 13C4.47905 13 3.99905 13.53 4.07905 14.13C4.69905 18.52 8.87905 21.77 13.6091 20.85C16.7291 20.24 19.2391 17.73 19.8491 14.61C20.8391 9.48003 16.9391 5.00003 11.9991 5.00003Z" fill="black"/>
                        </svg></a>
                    </div>
                </div>
            </div>
            <div class="dropdown-block">
                <span class="dropdown-block__title">Состав заказа</span>
                <div class="dropdown-boxes">
                    <? foreach($arOrder['BASKET_ITEMS'] as $arBasketItems): ?>
                        <div class="dropdown-box">
                            <div class="card-name">
                                <a class="stock stock-list" href="#">«Держи пять»</a>
                                <p><?= $arBasketItems['NAME'] ?></p>
                            </div>
                            <a class="in-favorite" href="#">
                                <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M7.00023 7.16667C4.20982 8.841 2.41689 12.1667 12.0001 19.6667C21.1666 12.1667 19.4999 9.25 17.8333 7.58334C15.8987 5.64883 13.2501 7.16667 12.0001 8.41667C10.7501 7.16667 9.08346 5.91667 7.00023 7.16667Z" stroke="#44A63D" stroke-width="2.5"/>
                                </svg>
                                <span>в «Мой ассортимент»</span>
                            </a>
                            <div class="card-info">
                                <div class="card-price">
                                    <p>по 5 шт.</p>
                                    <p><span><?= $arBasketItems['PRICE'] ?></span><span>руб.</span></p>
                                </div>
                            </div>
                        </div>
                    <? endforeach; ?>
                </div><a class="btn-green" href="?COPY_ORDER=Y&amp;ID=<?= $arOrder['ORDER']['ID'] ?>">Повторить заказ</a>
            </div>
        <? endforeach; ?>
    </div>
</div>
<?$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "",
    Array(
        "AREA_FILE_SHOW" => "file",
        "AREA_FILE_SUFFIX" => "inc",
        "EDIT_TEMPLATE" => "",
        "PATH" => "/include/personal/bottom.php"
    )
);?>

