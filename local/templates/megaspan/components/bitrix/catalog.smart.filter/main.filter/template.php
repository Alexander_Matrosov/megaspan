<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>
<div class="filter">
    <div class="filter-box"><span class="filter__title">Акция</span>
        <ul>
            <li>
                <label class="checkbox">
                    <input type="checkbox" name="checkbox"><span></span>
                    <p>Выгодные комплекты</p>
                </label>
            </li>
            <li>
                <label class="checkbox">
                    <input type="checkbox" name="checkbox"><span></span>
                    <p>Рассрочка или бонусы</p>
                </label>
            </li>
            <li>
                <label class="checkbox">
                    <input type="checkbox" name="checkbox"><span></span>
                    <p>Товары со скидкой</p>
                </label>
            </li>
        </ul>
    </div>
    <div class="filter-box"><span class="filter__title">Размер</span>
        <ul>
            <li>
                <label class="checkbox">
                    <input type="checkbox" name="checkbox"><span></span>
                    <p>1,6</p>
                </label>
            </li>
            <li>
                <label class="checkbox">
                    <input type="checkbox" name="checkbox"><span></span>
                    <p>2,6</p>
                </label>
            </li>
            <li>
                <label class="checkbox">
                    <input type="checkbox" name="checkbox"><span></span>
                    <p>3,6</p>
                </label>
            </li>
            <li>
                <label class="checkbox">
                    <input type="checkbox" name="checkbox"><span></span>
                    <p>4,6</p>
                </label>
            </li>
        </ul>
    </div>
    <div class="filter-box"><span class="filter__title">Название фильтра</span>
        <ul>
            <li>
                <label class="checkbox">
                    <input type="checkbox" name="checkbox"><span></span>
                    <p>1 Параметр</p>
                </label>
            </li>
            <li>
                <label class="checkbox">
                    <input type="checkbox" name="checkbox"><span></span>
                    <p>2 Параметр</p>
                </label>
            </li>
            <li>
                <label class="checkbox">
                    <input type="checkbox" name="checkbox"><span></span>
                    <p>3 Параметр</p>
                </label>
            </li>
            <li>
                <label class="checkbox">
                    <input type="checkbox" name="checkbox"><span></span>
                    <p>4 Параметр</p>
                </label>
            </li>
        </ul>
    </div>
    <div class="filter-btns"><a class="btn-apply" href="#">Применить</a><a class="btn-drop" href="#">Сбросить</a></div>
</div>
