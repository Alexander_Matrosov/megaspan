<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<span class="personal__user"><?= $arResult['arUser']['EMAIL'] ?></span>
<div class="user-data">
    <div class="input">
        <input type="password" required><span>Пароль</span>
    </div>
    <div class="input">
        <input type="text" required><span>Ф.И.О.</span>
    </div>
    <div class="input">
        <input type="text" required><span>Компания</span>
    </div>
    <div class="input">
        <input type="text" required><span>ИНН</span>
    </div>
    <div class="input">
        <input type="text" required><span>Телефон</span>
    </div>
</div>
<div class="user-data">
    <span class="user-data__title">Адрес</span>
    <div class="user-inputs">
        <div class="input">
            <input type="text" required><span>Город</span>
        </div>
        <div class="input">
            <input type="text" required><span>Улица</span>
        </div>
        <div class="input">
            <input type="text" required><span>Дом</span>
        </div>
        <div class="input">
            <input type="text" required><span>Офис/Склад</span>
        </div>
        <div class="textarea">
            <textarea name=""></textarea><span>Комментарий</span>
        </div>
    </div>
</div>
<div class="user-checkboxes">
    <span class="user-data__title">Уведомления</span>
    <label class="checkbox">
        <input type="checkbox" name="checkbox"><span></span>
        <p>Высылать счет после заявки</p>
    </label>
    <label class="checkbox">
        <input type="checkbox" name="checkbox"><span></span>
        <p>Озвучивать мне сумму по телефону</p>
    </label>
    <label class="checkbox">
        <input type="checkbox" name="checkbox"><span></span>
        <p>Хочу получать уведомление об отгрузке на телефон</p>
    </label>
    <label class="checkbox">
        <input type="checkbox" name="checkbox"><span></span>
        <p>Если отгрузка меняется по сравнению с выставленным счётом прошу мне звонить для уточнения изменения в заказе</p>
    </label>
    <label class="checkbox">
        <input type="checkbox" name="checkbox"><span></span>
        <p>Всегда уведомлять меня о новых товарах письмом на почту</p>
    </label>
</div>
<a class="btn-green" href="#">Сохранить</a>




