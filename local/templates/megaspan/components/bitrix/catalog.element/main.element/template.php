<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);
?>
<? if($arResult): ?>
    <div class="card-main">
        <div class="card-sliders">
            <div class="card-slider gallery-top">
                <div class="item-photos">
                    <div class="item-img" style="background-image: url(<?= $arResult['PREVIEW_PICTURE']['SRC'] ?>)"></div>
                </div>
            </div>
            <div class="card-slider gallery-thumbs">
                <div class="item-photos">
                    <div class="item-img" style="background-image: url(<?= $arResult['PREVIEW_PICTURE']['SRC'] ?>)"></div>
                </div>
                <div class="slider-arrow swiper-button-next"></div>
                <div class="slider-arrow swiper-button-prev"></div>
            </div>
        </div>
        <div class="card-information">
            <div class="card-information__top">
                <? foreach($arResult['PRICES'] as $priceCode => $arPrice): ?>
                    <div class="card-info">
                        <div class="card-box">
                            <p>цена</p><span><?= $arResult['CAT_PRICES'][$priceCode]['TITLE'] ?></span>
                        </div>
                        <div class="card-price">
                            <p> <span><?= $arPrice['VALUE'] ?></span><span>руб.</span></p>
                            <p>по 5 шт.</p>
                        </div>
                        <div class="card-opt"><svg width="17" height="17" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M8.33333 0C3.73833 0 0 3.73833 0 8.33333C0 12.9283 3.73833 16.6667 8.33333 16.6667C12.9283 16.6667 16.6667 12.9283 16.6667 8.33333C16.6667 3.73833 12.9283 0 8.33333 0ZM9.16667 12.5H7.5V7.5H9.16667V12.5ZM9.16667 5.83333H7.5V4.16667H9.16667V5.83333Z" fill="#CCCCCC"/>
                            </svg>
                        </div>
                    </div>
                <? endforeach; ?>
                <div class="card-footer">
                    <div class="card-count">
                        <span><svg width="8" height="2" viewBox="0 0 8 2" fill="none" xmlns="http://www.w3.org/2000/svg"><rect width="8" height="2" fill="#CCCCCC"/></svg></span>
                        <input type="text" value="0">
                        <span><svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M5 3V0H3V3H0V5H3V8H5V5H8V3H5Z" fill="#44A63D"/></svg></span>
                    </div>
                    <a class="btn" href="<?= $arResult['BUY_URL'] ?>">
                        <svg width="167" height="46" viewBox="0 0 167 46" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path class="basket-back" fill-rule="evenodd" clip-rule="evenodd" d="M0 46V9L9 0H28.6086H83.4418H166.884V37.1163L158 46L133.507 46H0Z" fill="#44A63D"/>
                            <path class="basket-text" d="M64.647 27.1428H68.326C70.7322 27.1428 71.7266 25.9695 71.7266 24.4382C71.7266 22.8274 70.6129 21.9524 69.6783 21.8928V21.7933C70.5533 21.5547 71.3288 20.978 71.3288 19.6655C71.3288 18.174 70.3345 16.9609 68.2067 16.9609H64.647V27.1428ZM65.88 26.049V22.5092H68.3857C69.718 22.5092 70.5533 23.4041 70.5533 24.4382C70.5533 25.3331 69.9368 26.049 68.326 26.049H65.88ZM65.88 21.4354V18.0547H68.2067C69.5589 18.0547 70.1555 18.7706 70.1555 19.6655C70.1555 20.7394 69.2805 21.4354 68.1669 21.4354H65.88ZM77.5309 27.1428H78.7042V23.9212H79.7582L82.2638 27.1428H83.7752L80.6531 23.1854L83.7354 19.5064H82.2241L79.4798 22.8274H78.7042V19.5064H77.5309V27.1428ZM87.8382 27.3019C89.9064 27.3019 91.2985 25.7308 91.2985 23.3644C91.2985 20.978 89.9064 19.407 87.8382 19.407C85.7701 19.407 84.378 20.978 84.378 23.3644C84.378 25.7308 85.7701 27.3019 87.8382 27.3019ZM87.8382 26.2479C86.2672 26.2479 85.5513 24.8956 85.5513 23.3644C85.5513 21.8331 86.2672 20.4609 87.8382 20.4609C89.4093 20.4609 90.1252 21.8331 90.1252 23.3644C90.1252 24.8956 89.4093 26.2479 87.8382 26.2479ZM93.0895 30.0064H94.2628V25.9695H94.3622C94.6207 26.3871 95.1179 27.3019 96.5895 27.3019C98.4986 27.3019 99.831 25.7706 99.831 23.3445C99.831 20.9382 98.4986 19.407 96.5696 19.407C95.0781 19.407 94.6207 20.3217 94.3622 20.7195H94.223V19.5064H93.0895V30.0064ZM94.2429 23.3246C94.2429 21.6144 94.9986 20.4609 96.4304 20.4609C97.9219 20.4609 98.6577 21.7138 98.6577 23.3246C98.6577 24.9553 97.902 26.2479 96.4304 26.2479C95.0185 26.2479 94.2429 25.0547 94.2429 23.3246ZM101.143 25.1939C101.183 26.4467 102.381 27.3019 104.007 27.3019C105.678 27.3019 106.891 26.407 106.891 25.0149C106.891 24.0604 106.319 23.2848 105.459 23.2251V23.1456C106.185 22.9219 106.732 22.3949 106.732 21.5348C106.732 20.3267 105.697 19.407 104.087 19.407C102.496 19.407 101.362 20.282 101.322 21.5149H102.555C102.59 20.8835 103.251 20.4411 104.146 20.4411C105.036 20.4411 105.538 20.8935 105.538 21.5746C105.538 22.2109 105.101 22.6882 104.166 22.6882H102.854V23.7422H104.166C105.101 23.7422 105.697 24.2543 105.697 25.0149C105.697 25.8849 104.927 26.2479 103.987 26.2479C103.067 26.2479 102.451 25.8849 102.396 25.1939H101.143ZM109.712 25.4126V19.5064H108.539V27.1428H109.911L113.57 21.2365V27.1428H114.743V19.5064H113.391L109.712 25.4126ZM118.066 19.5064H116.892V27.1428H118.066V23.8814H121.725V27.1428H122.898V19.5064H121.725V22.7876H118.066V19.5064ZM125.523 29.9865C126.582 29.9865 127.333 29.4247 127.77 28.2564L131.021 19.5263L129.739 19.5064L127.631 25.5916H127.551L125.443 19.5064H124.171L126.994 27.1825L126.776 27.7791C126.358 28.9226 125.821 29.032 124.966 28.8132L124.668 29.8473C124.787 29.907 125.125 29.9865 125.523 29.9865Z" fill="white"/>
                            <path class="basket-stroke" d="M34.7606 30.9639C35.1645 30.9639 35.4919 30.6313 35.4919 30.221C35.4919 29.8107 35.1645 29.4781 34.7606 29.4781C34.3567 29.4781 34.0293 29.8107 34.0293 30.221C34.0293 30.6313 34.3567 30.9639 34.7606 30.9639Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path class="basket-stroke" d="M42.8045 30.9639C43.2084 30.9639 43.5358 30.6313 43.5358 30.221C43.5358 29.8107 43.2084 29.4781 42.8045 29.4781C42.4006 29.4781 42.0732 29.8107 42.0732 30.221C42.0732 30.6313 42.4006 30.9639 42.8045 30.9639Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path class="basket-stroke" d="M28.9102 15.3631H31.8353L33.7951 25.3104C33.862 25.6525 34.0452 25.9597 34.3126 26.1784C34.58 26.397 34.9145 26.5132 35.2577 26.5065H42.3658C42.709 26.5132 43.0436 26.397 43.311 26.1784C43.5784 25.9597 43.7615 25.6525 43.8284 25.3104L44.9985 19.0776H32.5666" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </a>
                    <a class="in-favorite" href="#">
                        <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M7.00023 7.16667C4.20982 8.841 2.41689 12.1667 12.0001 19.6667C21.1666 12.1667 19.4999 9.25 17.8333 7.58334C15.8987 5.64883 13.2501 7.16667 12.0001 8.41667C10.7501 7.16667 9.08346 5.91667 7.00023 7.16667Z" stroke="#44A63D" stroke-width="2.5"/>
                        </svg>
                    </a>
                </div>
            </div>
            <p class="card-information__subtitle"><?= $arResult['PREVIEW_TEXT'] ?></p>
            <ul class="card-information__list">
                <? foreach($arResult['PROPERTIES'] as $arProperty): ?>
                    <? if($arProperty['VALUE']): ?>
                        <li><span><?= $arProperty['NAME'] ?></span><span><?= $arProperty['VALUE'] ?></span></li>
                    <? endif; ?>
                <? endforeach; ?>
            </ul>
        </div>
    </div>
<? endif; ?>
