<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

foreach($arResult['SECTIONS'] as $arSection){
    if($arSection['DEPTH_LEVEL'] == 1 && $arSection['ELEMENT_CNT'] >= 1){
        foreach($arResult['SECTIONS'] as $arSubsection){
            if($arSubsection['IBLOCK_SECTION_ID'] == $arSection['ID'] && $arSubsection['ELEMENT_CNT'] >= 1){
                foreach($arResult['SECTIONS'] as $arSubSubsection){
                    if($arSubSubsection['IBLOCK_SECTION_ID'] == $arSubsection['ID'] && $arSubSubsection['ELEMENT_CNT'] >= 1){
                        $arSubsection['SUBSUBSECTION'][] = $arSubSubsection;
                    }
                }
                $arSection['SUBSECTION'][] = $arSubsection;
            }
        }
        $arResult['WOOD_SECTION'][] = $arSection;
    }
}
?>