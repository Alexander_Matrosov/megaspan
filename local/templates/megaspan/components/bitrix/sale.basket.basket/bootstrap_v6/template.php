<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main;
use Bitrix\Main\Localization\Loc;

\Bitrix\Main\UI\Extension::load("ui.fonts.ruble");

/**
 * @var array $arParams
 * @var array $arResult
 * @var string $templateFolder
 * @var string $templateName
 * @var CMain $APPLICATION
 * @var CBitrixBasketComponent $component
 * @var CBitrixComponentTemplate $this
 * @var array $giftParameters
 */

?>
<div class="container">
    <div class="basket">
        <div class="breadcrumbs"><a href="#">Главная страница</a><a href="#">Мегаспан ГЕО 100</a></div>
        <h1 class="title">Корзина</h1>
        <div class="basket-main">
            <? if($arResult['GRID']['ROWS']): ?>
                <a class="basket-clear" href="<?=$APPLICATION->GetCurPage();?>?action=clear" onclick="return ClearCart(this);">Очистить корзину</a>
                <div class="basket-table">
                    <div class="basket-head">
                        <div>Товар</div>
                        <div class="text-center">Попробовать</div>
                        <div>Ед. изм.</div>
                        <div>Цена за ед.</div>
                        <div>Кол-во</div>
                        <div>Сумма</div>
                        <div> </div>
                    </div>
                    <div class="basket-list list-text">
                        <? foreach($arResult['GRID']['ROWS'] as $arBasketItems): ?>
                            <div class="card">
                                <div class="card-img"><img src="./img/card-img.jpg" alt=""></div>
                                <div class="card-name"><a class="stock stock-list" href="#">«Держи пять»</a>
                                    <p><?= $arBasketItems['NAME']?></p>
                                    <div class="card-box">
                                        <p>цена</p><span><?= $arBasketItems['NOTES'] ?></span><a class="stock stock-row" href="#">«Держи пять»</a>
                                    </div>
                                </div>
                                <div class="card-sample">
                                    <label class="checkbox">
                                        <input type="checkbox" name="checkbox"><span></span>
                                        <p>Попробовать</p>
                                    </label>
                                </div>
                                <div class="card-info">
                                    <div class="card-box">
                                        <p>Цена за ед.</p><span>м2</span>
                                    </div>
                                </div>
                                <div class="card-info">
                                    <div class="card-price">
                                        <p><span><?= $arBasketItems['PRICE']?></span><span>руб.</span></p>
                                        <p>по 5 шт.</p>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="card-count">
                                        <span data-minus><svg width="8" height="2" viewBox="0 0 8 2" fill="none" xmlns="http://www.w3.org/2000/svg"><rect width="8" height="2" fill="#CCCCCC"/></svg></span>
                                        <input data-quantity type="text" value="<?= $arBasketItems['QUANTITY']?>">
                                        <span data-plus><svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M5 3V0H3V3H0V5H3V8H5V5H8V3H5Z" fill="#44A63D"/></svg></span>
                                    </div>
                                </div>
                                <div class="card-info">
                                    <div class="card-price">
                                        <p> <span><?= $arBasketItems['SUM_VALUE'] ?></span><span>руб. </span></p>
                                    </div>
                                </div>
                                <a class="delete-btn" href="#">
                                    <svg width="13" height="14" viewBox="0 0 13 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <line y1="-0.5" x2="17.7496" y2="-0.5" transform="matrix(0.680862 0.732412 -0.680862 0.732412 0 1)" stroke="#AAA6A6"/>
                                        <line y1="-0.5" x2="17.7496" y2="-0.5" transform="matrix(-0.680862 0.732412 0.680862 0.732412 13 1)" stroke="#AAA6A6"/>
                                    </svg>
                                </a>
                            </div>
                        <? endforeach; ?>
                    </div>
                    <div class="basket-footer">
                        <div class="basket-footer__boxes">
                            <div class="basket-footer__box"><span>Вес:</span>
                                <p>550<span>кг</span></p>
                            </div>
                            <div class="basket-footer__box"><span>Объем:</span>
                                <p>3,5<span>м3</span></p>
                            </div>
                            <div class="basket-footer__box"><span>Сумма</span>
                                <p>432 123,00<span>р.</span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="delivery">
                    <? if(!$USER->IsAuthorized()): ?>
                        <div class="delivery-block">
                            <div class="delivery-login">
                                <a class="login-btn btn-blue" href="#" data-izimodal-open="#modal-personal" data-izimodal-transitionin="fadeInDown">Войти</a>
                                <span>Если вы зарегистрированы</span>
                            </div>
                            <div class="delivery-boxes">
                                <div class="delivery-box">
                                    <p>Способы получения</p>
                                    <div class="delivery-inputs">
                                        <select class="select" name="#">
                                            <option value="#">Доставка</option>
                                            <option value="#">Доставка</option>
                                            <option value="#">Доставка</option>
                                        </select>
                                        <div class="input">
                                            <input type="text" required><span>Город</span>
                                        </div>
                                        <div class="input">
                                            <input type="text" required><span>Улица</span>
                                        </div>
                                        <div class="input">
                                            <input type="text" required><span>Дом</span>
                                        </div>
                                        <div class="input">
                                            <input type="text" required><span>Офис</span>
                                        </div>
                                        <div class="input">
                                            <input type="text" required><span>Комментарий</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="delivery-box">
                                    <p>Способ оплаты</p>
                                    <select class="select" name="#">
                                        <option value="#">Безналичный расчёт</option>
                                        <option value="#">Безналичный расчёт</option>
                                        <option value="#">Безналичный расчёт</option>
                                    </select>
                                </div>
                                <div class="delivery-box">
                                    <p>Комментарий</p>
                                    <div class="input input-maxWidth">
                                        <input type="text" required><span>E-Mail</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <? else: ?>
                        <div class="delivery-block">
                            <div class="delivery-user">
                                <p>ООО «Покупатель»</p><a class="personal-btn btn-blue" href="#">Личный кабинет</a>
                            </div>
                            <div class="delivery-boxes">
                                <div class="delivery-box">
                                    <p>Способы получения</p>
                                    <div class="delivery-input">
                                        <div class="input">
                                            <input type="text" required><span>Адрес</span>
                                        </div><a class="delivery-input__icon" href="#"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M7 17.013L11.413 16.998L21.045 7.45802C21.423 7.08003 21.631 6.57802 21.631 6.04402C21.631 5.51002 21.423 5.00802 21.045 4.63002L19.459 3.04402C18.703 2.28802 17.384 2.29202 16.634 3.04102L7 12.583V17.013ZM18.045 4.45802L19.634 6.04102L18.037 7.62302L16.451 6.03802L18.045 4.45802ZM9 13.417L15.03 7.44402L16.616 9.03002L10.587 15.001L9 15.006V13.417Z" fill="#44A63D"/>
                                                <path d="M5 21H19C20.103 21 21 20.103 21 19V10.332L19 12.332V19H8.158C8.132 19 8.105 19.01 8.079 19.01C8.046 19.01 8.013 19.001 7.979 19H5V5H11.847L13.847 3H5C3.897 3 3 3.897 3 5V19C3 20.103 3.897 21 5 21Z" fill="#44A63D"/>
                                            </svg></a>
                                    </div>
                                </div>
                                <div class="delivery-box">
                                    <p>Способ оплаты</p>
                                    <select class="select" name="#">
                                        <option value="#">Безналичный расчёт</option>
                                        <option value="#">Безналичный расчёт</option>
                                        <option value="#">Безналичный расчёт</option>
                                    </select>
                                </div>
                                <div class="delivery-box">
                                    <p>Комментарий</p>
                                    <div class="input input-maxWidth">
                                        <input type="text" required><span>E-Mail</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <? endif; ?>


                    <a class="delivery-btn btn-green" href="#">Оформить заказ</a>
                </div>
            <? else: ?>
                <span>Ваша корзина пуста</span>
            <? endif; ?>
        </div>
    </div>
</div>

