<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main;
use Bitrix\Main\Localization\Loc;

\Bitrix\Main\UI\Extension::load("ui.fonts.ruble");

/**
 * @var array $arParams
 * @var array $arResult
 * @var string $templateFolder
 * @var string $templateName
 * @var CMain $APPLICATION
 * @var CBitrixBasketComponent $component
 * @var CBitrixComponentTemplate $this
 * @var array $giftParameters
 */

if (!isset($arParams['DISPLAY_MODE']) || !in_array($arParams['DISPLAY_MODE'], array('extended', 'compact')))
{
	$arParams['DISPLAY_MODE'] = 'extended';
}

$arParams['USE_DYNAMIC_SCROLL'] = isset($arParams['USE_DYNAMIC_SCROLL']) && $arParams['USE_DYNAMIC_SCROLL'] === 'N' ? 'N' : 'Y';
$arParams['SHOW_FILTER'] = isset($arParams['SHOW_FILTER']) && $arParams['SHOW_FILTER'] === 'N' ? 'N' : 'Y';

$arParams['PRICE_DISPLAY_MODE'] = isset($arParams['PRICE_DISPLAY_MODE']) && $arParams['PRICE_DISPLAY_MODE'] === 'N' ? 'N' : 'Y';

if (!isset($arParams['TOTAL_BLOCK_DISPLAY']) || !is_array($arParams['TOTAL_BLOCK_DISPLAY']))
{
	$arParams['TOTAL_BLOCK_DISPLAY'] = array('top');
}

if (empty($arParams['PRODUCT_BLOCKS_ORDER']))
{
	$arParams['PRODUCT_BLOCKS_ORDER'] = 'props,sku,columns';
}

if (is_string($arParams['PRODUCT_BLOCKS_ORDER']))
{
	$arParams['PRODUCT_BLOCKS_ORDER'] = explode(',', $arParams['PRODUCT_BLOCKS_ORDER']);
}

$arParams['USE_PRICE_ANIMATION'] = isset($arParams['USE_PRICE_ANIMATION']) && $arParams['USE_PRICE_ANIMATION'] === 'N' ? 'N' : 'Y';
$arParams['EMPTY_BASKET_HINT_PATH'] = isset($arParams['EMPTY_BASKET_HINT_PATH']) ? (string)$arParams['EMPTY_BASKET_HINT_PATH'] : '/';
$arParams['USE_ENHANCED_ECOMMERCE'] = isset($arParams['USE_ENHANCED_ECOMMERCE']) && $arParams['USE_ENHANCED_ECOMMERCE'] === 'Y' ? 'Y' : 'N';
$arParams['DATA_LAYER_NAME'] = isset($arParams['DATA_LAYER_NAME']) ? trim($arParams['DATA_LAYER_NAME']) : 'dataLayer';
$arParams['BRAND_PROPERTY'] = isset($arParams['BRAND_PROPERTY']) ? trim($arParams['BRAND_PROPERTY']) : '';

if ($arParams['USE_GIFTS'] === 'Y')
{
	$arParams['GIFTS_BLOCK_TITLE'] = isset($arParams['GIFTS_BLOCK_TITLE']) ? trim((string)$arParams['GIFTS_BLOCK_TITLE']) : Loc::getMessage('SBB_GIFTS_BLOCK_TITLE');

	CBitrixComponent::includeComponentClass('bitrix:sale.products.gift.basket');

	$giftParameters = array(
		'SHOW_PRICE_COUNT' => 1,
		'PRODUCT_SUBSCRIPTION' => 'N',
		'PRODUCT_ID_VARIABLE' => 'id',
		'USE_PRODUCT_QUANTITY' => 'N',
		'ACTION_VARIABLE' => 'actionGift',
		'ADD_PROPERTIES_TO_BASKET' => 'Y',
		'PARTIAL_PRODUCT_PROPERTIES' => 'Y',

		'BASKET_URL' => $APPLICATION->GetCurPage(),
		'APPLIED_DISCOUNT_LIST' => $arResult['APPLIED_DISCOUNT_LIST'],
		'FULL_DISCOUNT_LIST' => $arResult['FULL_DISCOUNT_LIST'],

		'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
		'PRICE_VAT_INCLUDE' => $arParams['PRICE_VAT_SHOW_VALUE'],
		'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],

		'BLOCK_TITLE' => $arParams['GIFTS_BLOCK_TITLE'],
		'HIDE_BLOCK_TITLE' => $arParams['GIFTS_HIDE_BLOCK_TITLE'],
		'TEXT_LABEL_GIFT' => $arParams['GIFTS_TEXT_LABEL_GIFT'],

		'DETAIL_URL' => isset($arParams['GIFTS_DETAIL_URL']) ? $arParams['GIFTS_DETAIL_URL'] : null,
		'PRODUCT_QUANTITY_VARIABLE' => $arParams['GIFTS_PRODUCT_QUANTITY_VARIABLE'],
		'PRODUCT_PROPS_VARIABLE' => $arParams['GIFTS_PRODUCT_PROPS_VARIABLE'],
		'SHOW_OLD_PRICE' => $arParams['GIFTS_SHOW_OLD_PRICE'],
		'SHOW_DISCOUNT_PERCENT' => $arParams['GIFTS_SHOW_DISCOUNT_PERCENT'],
		'DISCOUNT_PERCENT_POSITION' => $arParams['DISCOUNT_PERCENT_POSITION'],
		'MESS_BTN_BUY' => $arParams['GIFTS_MESS_BTN_BUY'],
		'MESS_BTN_DETAIL' => $arParams['GIFTS_MESS_BTN_DETAIL'],
		'CONVERT_CURRENCY' => $arParams['GIFTS_CONVERT_CURRENCY'],
		'HIDE_NOT_AVAILABLE' => $arParams['GIFTS_HIDE_NOT_AVAILABLE'],

		'PRODUCT_ROW_VARIANTS' => '',
		'PAGE_ELEMENT_COUNT' => 0,
		'DEFERRED_PRODUCT_ROW_VARIANTS' => \Bitrix\Main\Web\Json::encode(
			SaleProductsGiftBasketComponent::predictRowVariants(
				$arParams['GIFTS_PAGE_ELEMENT_COUNT'],
				$arParams['GIFTS_PAGE_ELEMENT_COUNT']
			)
		),
		'DEFERRED_PAGE_ELEMENT_COUNT' => $arParams['GIFTS_PAGE_ELEMENT_COUNT'],

		'ADD_TO_BASKET_ACTION' => 'BUY',
		'PRODUCT_DISPLAY_MODE' => 'Y',
		'PRODUCT_BLOCKS_ORDER' => isset($arParams['GIFTS_PRODUCT_BLOCKS_ORDER']) ? $arParams['GIFTS_PRODUCT_BLOCKS_ORDER'] : '',
		'SHOW_SLIDER' => isset($arParams['GIFTS_SHOW_SLIDER']) ? $arParams['GIFTS_SHOW_SLIDER'] : '',
		'SLIDER_INTERVAL' => isset($arParams['GIFTS_SLIDER_INTERVAL']) ? $arParams['GIFTS_SLIDER_INTERVAL'] : '',
		'SLIDER_PROGRESS' => isset($arParams['GIFTS_SLIDER_PROGRESS']) ? $arParams['GIFTS_SLIDER_PROGRESS'] : '',
		'LABEL_PROP_POSITION' => $arParams['LABEL_PROP_POSITION'],

		'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
		'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
		'BRAND_PROPERTY' => $arParams['BRAND_PROPERTY']
	);
}

\CJSCore::Init(array('fx', 'popup', 'ajax'));

//$this->addExternalCss($templateFolder.'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css');

$this->addExternalJs($templateFolder.'/js/mustache.js');
$this->addExternalJs($templateFolder.'/js/action-pool.js');
$this->addExternalJs($templateFolder.'/js/filter.js');
$this->addExternalJs($templateFolder.'/js/component.js');

$mobileColumns = isset($arParams['COLUMNS_LIST_MOBILE'])
	? $arParams['COLUMNS_LIST_MOBILE']
	: $arParams['COLUMNS_LIST'];
$mobileColumns = array_fill_keys($mobileColumns, true);

$jsTemplates = new Main\IO\Directory(Main\Application::getDocumentRoot().$templateFolder.'/js-templates');
/** @var Main\IO\File $jsTemplate */
foreach ($jsTemplates->getChildren() as $jsTemplate)
{
	include($jsTemplate->getPath());
}

$displayModeClass = $arParams['DISPLAY_MODE'] === 'compact' ? ' basket-items-list-wrapper-compact' : '';

if (empty($arResult['ERROR_MESSAGE']))
{
	if ($arResult['BASKET_ITEM_MAX_COUNT_EXCEEDED'])
	{
		?>
		<div id="basket-item-message">
			<?=Loc::getMessage('SBB_BASKET_ITEM_MAX_COUNT_EXCEEDED', array('#PATH#' => $arParams['PATH_TO_BASKET']))?>
		</div>
		<?
	}
	?>
	<div id="basket-root" class="bx-basket bx-<?=$arParams['TEMPLATE_THEME']?> bx-step-opacity" style="opacity: 0;">
		<?
		if (
			$arParams['BASKET_WITH_ORDER_INTEGRATION'] !== 'Y'
			&& in_array('top', $arParams['TOTAL_BLOCK_DISPLAY'])
		)
		{
			?>
			<div class="row">
				<div class="col" data-entity="basket-total-block"></div>
			</div>
			<?
		}
		?>

		<div class="row">
			<div class="col">
				<div class="alert alert-warning alert-dismissable" id="basket-warning" style="display: none;">
					<span class="close" data-entity="basket-items-warning-notification-close">&times;</span>
					<div data-entity="basket-general-warnings"></div>
					<div data-entity="basket-item-warnings">
						<?=Loc::getMessage('SBB_BASKET_ITEM_WARNING')?>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col">
				<div class="mb-3 basket-items-list-wrapper basket-items-list-wrapper-height-fixed basket-items-list-wrapper-light<?=$displayModeClass?>"
					id="basket-items-list-wrapper">
					<div class="basket-items-list-header" data-entity="basket-items-list-header">
						<div class="basket-items-search-field" data-entity="basket-filter">
							<div class="form input-group">
								<input type="text" class="form-control" placeholder="<?=Loc::getMessage('SBB_BASKET_FILTER')?>" data-entity="basket-filter-input">
							</div>
							<button class="basket-items-search-clear-btn" type="button" data-entity="basket-filter-clear-btn">&times;</button>
						</div>

						<div class="basket-items-list-header-filter">
							<a href="javascript:void(0)" class="basket-items-list-header-filter-item active" data-entity="basket-items-count" data-filter="all" style="display: none;"></a>
							<a href="javascript:void(0)" class="basket-items-list-header-filter-item" data-entity="basket-items-count" data-filter="similar" style="display: none;"></a>
							<a href="javascript:void(0)" class="basket-items-list-header-filter-item" data-entity="basket-items-count" data-filter="warning" style="display: none;"></a>
							<a href="javascript:void(0)" class="basket-items-list-header-filter-item" data-entity="basket-items-count" data-filter="delayed" style="display: none;"></a>
							<a href="javascript:void(0)" class="basket-items-list-header-filter-item" data-entity="basket-items-count" data-filter="not-available" style="display: none;"></a>
						</div>
					</div>
					<div class="basket-items-list-container" id="basket-items-list-container">
						<div class="basket-items-list-overlay" id="basket-items-list-overlay" style="display: none;"></div>
						<div class="basket-items-list" id="basket-item-list">
							<div class="basket-search-not-found" id="basket-item-list-empty-result" style="display: none;">
								<div class="basket-search-not-found-icon"></div>
								<div class="basket-search-not-found-text"><?=Loc::getMessage('SBB_FILTER_EMPTY_RESULT')?></div>
							</div>
							<table class="basket-items-list-table" id="basket-item-table"></table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?
		if (
			$arParams['BASKET_WITH_ORDER_INTEGRATION'] !== 'Y'
			&& in_array('bottom', $arParams['TOTAL_BLOCK_DISPLAY'])
		)
		{
			?>
			<div class="row">
				<div class="col" data-entity="basket-total-block"></div>
			</div>
			<?
		}
		?>
	</div>
	<?
	if (!empty($arResult['CURRENCIES']) && Main\Loader::includeModule('currency'))
	{
		CJSCore::Init('currency');

		?>
		<script>
			BX.Currency.setCurrencies(<?=CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true)?>);
		</script>
		<?
	}

	$signer = new \Bitrix\Main\Security\Sign\Signer;
	$signedTemplate = $signer->sign($templateName, 'sale.basket.basket');
	$signedParams = $signer->sign(base64_encode(serialize($arParams)), 'sale.basket.basket');
	$messages = Loc::loadLanguageFile(__FILE__);
	?>
	<script>
		BX.message(<?=CUtil::PhpToJSObject($messages)?>);
		BX.Sale.BasketComponent.init({
			result: <?=CUtil::PhpToJSObject($arResult, false, false, true)?>,
			params: <?=CUtil::PhpToJSObject($arParams)?>,
			template: '<?=CUtil::JSEscape($signedTemplate)?>',
			signedParamsString: '<?=CUtil::JSEscape($signedParams)?>',
			siteId: '<?=CUtil::JSEscape($component->getSiteId())?>',
			siteTemplateId: '<?=CUtil::JSEscape($component->getSiteTemplateId())?>',
			templateFolder: '<?=CUtil::JSEscape($templateFolder)?>'
		});
	</script>
	<?
}
elseif ($arResult['EMPTY_BASKET'])
{
	include(Main\Application::getDocumentRoot().$templateFolder.'/empty.php');
}
else
{
	ShowError($arResult['ERROR_MESSAGE']);
}
?>
<div class="container">
    <div class="basket">
        <div class="breadcrumbs"><a href="#">Главная страница</a><a href="#">Мегаспан ГЕО 100</a></div>
        <h1 class="title">Корзина</h1>
        <div class="basket-main"><a class="basket-clear" href="#">Очистить корзину</a>
            <div class="basket-table">
                <div class="basket-head">
                    <div>Товар</div>
                    <div class="text-center">Попробовать</div>
                    <div>Ед. изм.</div>
                    <div>Цена за ед.</div>
                    <div>Кол-во</div>
                    <div>Сумма</div>
                    <div> </div>
                </div>
                <div class="basket-list list-text">
                    <div class="card">
                        <div class="card-img"><img src="./img/card-img.jpg" alt=""></div>
                        <div class="card-name"><a class="stock stock-list" href="#">«Держи пять»</a>
                            <p>МЕГАСПАН ГЕО 100 - 40м2 (1,6м*25п.м.) под тротуарную плитку</p>
                            <div class="card-box">
                                <p>цена</p><span>м2</span><a class="stock stock-row" href="#">«Держи пять»</a>
                            </div>
                        </div>
                        <div class="card-sample">
                            <label class="checkbox">
                                <input type="checkbox" name="checkbox"><span></span>
                                <p>Попробовать</p>
                            </label>
                        </div>
                        <div class="card-info">
                            <div class="card-box">
                                <p>Цена за ед.</p><span>м2</span>
                            </div>
                        </div>
                        <div class="card-info">
                            <div class="card-price">
                                <p> <span>1256,53</span><span>руб.</span></p>
                                <p>по 5 шт.</p>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="card-count"><span>
                         <svg width="8" height="2" viewBox="0 0 8 2" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect width="8" height="2" fill="#CCCCCC"/>
</svg></span>
                                <input type="text" value="0"><span>
                         <svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M5 3V0H3V3H0V5H3V8H5V5H8V3H5Z" fill="#44A63D"/>
</svg></span>
                            </div>
                        </div>
                        <div class="card-info">
                            <div class="card-price">
                                <p> <span>656489,53</span><span>руб. </span></p>
                            </div>
                        </div><a class="delete-btn" href="#"><svg width="13" height="14" viewBox="0 0 13 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <line y1="-0.5" x2="17.7496" y2="-0.5" transform="matrix(0.680862 0.732412 -0.680862 0.732412 0 1)" stroke="#AAA6A6"/>
                                <line y1="-0.5" x2="17.7496" y2="-0.5" transform="matrix(-0.680862 0.732412 0.680862 0.732412 13 1)" stroke="#AAA6A6"/>
                            </svg></a>
                    </div>
                    <div class="card">
                        <div class="card-img"><img src="./imgimg/card-img.jpg" alt=""></div>
                        <div class="card-name"><a class="stock stock-list" href="#">«Держи пять»</a>
                            <p>МЕГАСПАН ГЕО 100 - 40м2 (1,6м*25п.м.) под тротуарную плитку</p>
                            <div class="card-box">
                                <p>цена</p><span>м2</span><a class="stock stock-row" href="#">«Держи пять»</a>
                            </div>
                        </div>
                        <div class="card-sample">
                            <label class="checkbox">
                                <input type="checkbox" name="checkbox"><span></span>
                                <p>Попробовать</p>
                            </label>
                        </div>
                        <div class="card-info">
                            <div class="card-box">
                                <p>Цена за ед.</p><span>м2</span>
                            </div>
                        </div>
                        <div class="card-info">
                            <div class="card-price">
                                <p> <span>132356,53</span><span>руб.</span></p>
                                <p>по 5 шт.</p>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="card-count"><span>
                         <svg width="8" height="2" viewBox="0 0 8 2" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect width="8" height="2" fill="#CCCCCC"/>
</svg></span>
                                <input type="text" value="0"><span>
                         <svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M5 3V0H3V3H0V5H3V8H5V5H8V3H5Z" fill="#44A63D"/>
</svg></span>
                            </div>
                        </div>
                        <div class="card-info">
                            <div class="card-price">
                                <p> <span>489,53</span><span>руб. </span></p>
                            </div>
                        </div><a class="delete-btn" href="#"><svg width="13" height="14" viewBox="0 0 13 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <line y1="-0.5" x2="17.7496" y2="-0.5" transform="matrix(0.680862 0.732412 -0.680862 0.732412 0 1)" stroke="#AAA6A6"/>
                                <line y1="-0.5" x2="17.7496" y2="-0.5" transform="matrix(-0.680862 0.732412 0.680862 0.732412 13 1)" stroke="#AAA6A6"/>
                            </svg></a>
                    </div>
                    <div class="card">
                        <div class="card-img"><img src="./img/card-img.jpg" alt=""></div>
                        <div class="card-name"><a class="stock stock-list" href="#">«Держи пять»</a>
                            <p>МЕГАСПАН ГЕО 100 - 40м2 (1,6м*25п.м.) под тротуарную плитку</p>
                            <div class="card-box">
                                <p>цена</p><span>м2</span><a class="stock stock-row" href="#">«Держи пять»</a>
                            </div>
                        </div>
                        <div class="card-sample">
                            <label class="checkbox">
                                <input type="checkbox" name="checkbox"><span></span>
                                <p>Попробовать</p>
                            </label>
                        </div>
                        <div class="card-info">
                            <div class="card-box">
                                <p>Цена за ед.</p><span>м2</span>
                            </div>
                        </div>
                        <div class="card-info">
                            <div class="card-price">
                                <p> <span>13256,53</span><span>руб.</span></p>
                                <p>по 5 шт.</p>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="card-count"><span>
                         <svg width="8" height="2" viewBox="0 0 8 2" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect width="8" height="2" fill="#CCCCCC"/>
</svg></span>
                                <input type="text" value="0"><span>
                         <svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M5 3V0H3V3H0V5H3V8H5V5H8V3H5Z" fill="#44A63D"/>
</svg></span>
                            </div>
                        </div>
                        <div class="card-info">
                            <div class="card-price">
                                <p> <span>65489,53</span><span>руб. </span></p>
                            </div>
                        </div><a class="delete-btn" href="#"><svg width="13" height="14" viewBox="0 0 13 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <line y1="-0.5" x2="17.7496" y2="-0.5" transform="matrix(0.680862 0.732412 -0.680862 0.732412 0 1)" stroke="#AAA6A6"/>
                                <line y1="-0.5" x2="17.7496" y2="-0.5" transform="matrix(-0.680862 0.732412 0.680862 0.732412 13 1)" stroke="#AAA6A6"/>
                            </svg></a>
                    </div>
                    <div class="card">
                        <div class="card-img"><img src="./img/card-img.jpg" alt=""></div>
                        <div class="card-name"><a class="stock stock-list" href="#">«Держи пять»</a>
                            <p>МЕГАСПАН ГЕО 100 - 40м2 (1,6м*25п.м.) под тротуарную плитку</p>
                            <div class="card-box">
                                <p>цена</p><span>м2</span><a class="stock stock-row" href="#">«Держи пять»</a>
                            </div>
                        </div>
                        <div class="card-sample">
                            <label class="checkbox">
                                <input type="checkbox" name="checkbox"><span></span>
                                <p>Попробовать</p>
                            </label>
                        </div>
                        <div class="card-info">
                            <div class="card-box">
                                <p>Цена за ед.</p><span>м2</span>
                            </div>
                        </div>
                        <div class="card-info">
                            <div class="card-price">
                                <p> <span>13256,53</span><span>руб.</span></p>
                                <p>по 5 шт.</p>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="card-count"><span>
                         <svg width="8" height="2" viewBox="0 0 8 2" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect width="8" height="2" fill="#CCCCCC"/>
</svg></span>
                                <input type="text" value="0"><span>
                         <svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M5 3V0H3V3H0V5H3V8H5V5H8V3H5Z" fill="#44A63D"/>
</svg></span>
                            </div>
                        </div>
                        <div class="card-info">
                            <div class="card-price">
                                <p> <span>65489,53</span><span>руб. </span></p>
                            </div>
                        </div><a class="delete-btn" href="#"><svg width="13" height="14" viewBox="0 0 13 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <line y1="-0.5" x2="17.7496" y2="-0.5" transform="matrix(0.680862 0.732412 -0.680862 0.732412 0 1)" stroke="#AAA6A6"/>
                                <line y1="-0.5" x2="17.7496" y2="-0.5" transform="matrix(-0.680862 0.732412 0.680862 0.732412 13 1)" stroke="#AAA6A6"/>
                            </svg></a>
                    </div>
                </div>
                <div class="basket-footer">
                    <div class="basket-footer__boxes">
                        <div class="basket-footer__box"><span>Вес:</span>
                            <p>550<span>кг</span></p>
                        </div>
                        <div class="basket-footer__box"><span>Вес:</span>
                            <p>3,5<span>м3</span></p>
                        </div>
                        <div class="basket-footer__box"><span>Сумма</span>
                            <p>432 123,00<span>р.</span></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="delivery">
                <div class="delivery-block">
                    <div class="delivery-login"><a class="login-btn btn-blue" href="#">Войти</a><span>Если вы зарегистрированы</span></div>
                    <div class="delivery-boxes">
                        <div class="delivery-box">
                            <p>Способы получения</p>
                            <div class="delivery-inputs">
                                <select class="select" name="#">
                                    <option value="#">Доставка</option>
                                    <option value="#">Доставка</option>
                                    <option value="#">Доставка</option>
                                </select>
                                <div class="input">
                                    <input type="text" required><span>Город</span>
                                </div>
                                <div class="input">
                                    <input type="text" required><span>Улица</span>
                                </div>
                                <div class="input">
                                    <input type="text" required><span>Дом</span>
                                </div>
                                <div class="input">
                                    <input type="text" required><span>Офис</span>
                                </div>
                                <div class="input">
                                    <input type="text" required><span>Комментарий</span>
                                </div>
                            </div>
                        </div>
                        <div class="delivery-box">
                            <p>Способ оплаты</p>
                            <select class="select" name="#">
                                <option value="#">Безналичный расчёт</option>
                                <option value="#">Безналичный расчёт</option>
                                <option value="#">Безналичный расчёт</option>
                            </select>
                        </div>
                        <div class="delivery-box">
                            <p>Комментарий</p>
                            <div class="input input-maxWidth">
                                <input type="text" required><span>E-Mail</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="delivery-block">
                    <div class="delivery-user">
                        <p>ООО «Покупатель»</p><a class="personal-btn btn-blue" href="#">Личный кабинет</a>
                    </div>
                    <div class="delivery-boxes">
                        <div class="delivery-box">
                            <p>Способы получения</p>
                            <div class="delivery-input">
                                <div class="input">
                                    <input type="text" required><span>Адрес</span>
                                </div><a class="delivery-input__icon" href="#"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M7 17.013L11.413 16.998L21.045 7.45802C21.423 7.08003 21.631 6.57802 21.631 6.04402C21.631 5.51002 21.423 5.00802 21.045 4.63002L19.459 3.04402C18.703 2.28802 17.384 2.29202 16.634 3.04102L7 12.583V17.013ZM18.045 4.45802L19.634 6.04102L18.037 7.62302L16.451 6.03802L18.045 4.45802ZM9 13.417L15.03 7.44402L16.616 9.03002L10.587 15.001L9 15.006V13.417Z" fill="#44A63D"/>
                                        <path d="M5 21H19C20.103 21 21 20.103 21 19V10.332L19 12.332V19H8.158C8.132 19 8.105 19.01 8.079 19.01C8.046 19.01 8.013 19.001 7.979 19H5V5H11.847L13.847 3H5C3.897 3 3 3.897 3 5V19C3 20.103 3.897 21 5 21Z" fill="#44A63D"/>
                                    </svg></a>
                            </div>
                        </div>
                        <div class="delivery-box">
                            <p>Способ оплаты</p>
                            <select class="select" name="#">
                                <option value="#">Безналичный расчёт</option>
                                <option value="#">Безналичный расчёт</option>
                                <option value="#">Безналичный расчёт</option>
                            </select>
                        </div>
                        <div class="delivery-box">
                            <p>Комментарий</p>
                            <div class="input input-maxWidth">
                                <input type="text" required><span>E-Mail</span>
                            </div>
                        </div>
                    </div>
                </div><a class="delivery-btn btn-green" href="#">Оформить заказ</a>
            </div>
        </div>
    </div>
</div>
