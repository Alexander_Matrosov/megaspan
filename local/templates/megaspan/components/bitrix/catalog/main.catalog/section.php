<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;

$this->setFrameMode(true);

if (!isset($arParams['FILTER_VIEW_MODE']) || (string)$arParams['FILTER_VIEW_MODE'] == '')
	$arParams['FILTER_VIEW_MODE'] = 'VERTICAL';
$arParams['USE_FILTER'] = (isset($arParams['USE_FILTER']) && $arParams['USE_FILTER'] == 'Y' ? 'Y' : 'N');

$isFilter = ($arParams['USE_FILTER'] == 'Y');

if ($isFilter)
{
	$arFilter = array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ACTIVE" => "Y",
		"GLOBAL_ACTIVE" => "Y",
	);
	if (0 < intval($arResult["VARIABLES"]["SECTION_ID"]))
		$arFilter["ID"] = $arResult["VARIABLES"]["SECTION_ID"];
	elseif ('' != $arResult["VARIABLES"]["SECTION_CODE"])
		$arFilter["=CODE"] = $arResult["VARIABLES"]["SECTION_CODE"];

	$obCache = new CPHPCache();
	if ($obCache->InitCache(36000, serialize($arFilter), "/iblock/catalog"))
	{
		$arCurSection = $obCache->GetVars();
	}
	elseif ($obCache->StartDataCache())
	{
		$arCurSection = array();
		if (Loader::includeModule("iblock"))
		{
			$dbRes = CIBlockSection::GetList(array(), $arFilter, false, array("ID"));

			if(defined("BX_COMP_MANAGED_CACHE"))
			{
				global $CACHE_MANAGER;
				$CACHE_MANAGER->StartTagCache("/iblock/catalog");

				if ($arCurSection = $dbRes->Fetch())
					$CACHE_MANAGER->RegisterTag("iblock_id_".$arParams["IBLOCK_ID"]);

				$CACHE_MANAGER->EndTagCache();
			}
			else
			{
				if(!$arCurSection = $dbRes->Fetch())
					$arCurSection = array();
			}
		}
		$obCache->EndDataCache($arCurSection);
	}
	if (!isset($arCurSection))
		$arCurSection = array();
}
?>
<div class="container">
    <div class="catalog">
        <?$APPLICATION->IncludeComponent("bitrix:catalog.smart.filter", "main.filter", Array(
            "CACHE_GROUPS" => "Y",	// Учитывать права доступа
            "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
            "CACHE_TYPE" => "A",	// Тип кеширования
            "CONVERT_CURRENCY" => "N",	// Показывать цены в одной валюте
            "DISPLAY_ELEMENT_COUNT" => "Y",	// Показывать количество
            "FILTER_NAME" => "arrFilter",	// Имя выходящего массива для фильтрации
            "FILTER_VIEW_MODE" => "vertical",	// Вид отображения
            "HIDE_NOT_AVAILABLE" => "N",	// Не отображать недоступные товары
            "IBLOCK_ID" => "",	// Инфоблок
            "IBLOCK_TYPE" => "",	// Тип инфоблока
            "PAGER_PARAMS_NAME" => "arrPager",	// Имя массива с переменными для построения ссылок в постраничной навигации
            "PREFILTER_NAME" => "smartPreFilter",	// Имя входящего массива для дополнительной фильтрации элементов
            "PRICE_CODE" => "",	// Тип цены
            "SAVE_IN_SESSION" => "N",	// Сохранять установки фильтра в сессии пользователя
            "SECTION_CODE" => "",	// Код раздела
            "SECTION_DESCRIPTION" => "-",	// Описание
            "SECTION_ID" => $_REQUEST["SECTION_ID"],	// ID раздела инфоблока
            "SECTION_TITLE" => "-",	// Заголовок
            "SEF_MODE" => "N",	// Включить поддержку ЧПУ
            "TEMPLATE_THEME" => "blue",	// Цветовая тема
            "XML_EXPORT" => "N",	// Включить поддержку Яндекс Островов
        ),
            false
        );?>
        <?$APPLICATION->IncludeComponent("bitrix:catalog.section", "main.list", Array(
            "ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
            "ADD_PICT_PROP" => "-",	// Дополнительная картинка основного товара
            "ADD_PROPERTIES_TO_BASKET" => "Y",	// Добавлять в корзину свойства товаров и предложений
            "ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
            "ADD_TO_BASKET_ACTION" => "ADD",	// Показывать кнопку добавления в корзину или покупки
            "AJAX_MODE" => "N",	// Включить режим AJAX
            "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
            "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
            "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
            "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
            "BACKGROUND_IMAGE" => "-",	// Установить фоновую картинку для шаблона из свойства
            "BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
            "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
            "CACHE_GROUPS" => "N",	// Учитывать права доступа
            "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
            "CACHE_TYPE" => "N",	// Тип кеширования
            "COMPATIBLE_MODE" => "Y",	// Включить режим совместимости
            "CONVERT_CURRENCY" => "N",	// Показывать цены в одной валюте
            "CUSTOM_FILTER" => "{\"CLASS_ID\":\"CondGroup\",\"DATA\":{\"All\":\"AND\",\"True\":\"True\"},\"CHILDREN\":[]}",
            "DISABLE_INIT_JS_IN_COMPONENT" => "N",	// Не подключать js-библиотеки в компоненте
            "DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
            "DISPLAY_COMPARE" => "N",	// Разрешить сравнение товаров
            "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
            "ELEMENT_SORT_FIELD" => $arParams['ELEMENT_SORT_FIELD'],	// По какому полю сортируем элементы
            "ELEMENT_SORT_FIELD2" =>  $arParams['ELEMENT_SORT_FIELD2'],	// Поле для второй сортировки элементов
            "ELEMENT_SORT_ORDER" =>  $arParams['ELEMENT_SORT_ORDER'],	// Порядок сортировки элементов
            "ELEMENT_SORT_ORDER2" =>  $arParams['ELEMENT_SORT_ORDER2'],	// Порядок второй сортировки элементов
            "ENLARGE_PRODUCT" => "STRICT",	// Выделять товары в списке
            "FILTER_NAME" => "arrFilter",	// Имя массива со значениями фильтра для фильтрации элементов
            "HIDE_NOT_AVAILABLE" => "N",	// Недоступные товары
            "HIDE_NOT_AVAILABLE_OFFERS" => "N",	// Недоступные торговые предложения
            "IBLOCK_ID" => "4",	// Инфоблок
            "IBLOCK_TYPE" => "Content",	// Тип инфоблока
            "INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
            "LABEL_PROP" => "",	// Свойства меток товара
            "LAZY_LOAD" => "N",	// Показать кнопку ленивой загрузки Lazy Load
            "LINE_ELEMENT_COUNT" => "3",	// Количество элементов выводимых в одной строке таблицы
            "LOAD_ON_SCROLL" => "N",	// Подгружать товары при прокрутке до конца
            "MESS_BTN_ADD_TO_BASKET" => "В корзину",	// Текст кнопки "Добавить в корзину"
            "MESS_BTN_BUY" => "Купить",	// Текст кнопки "Купить"
            "MESS_BTN_DETAIL" => "Подробнее",	// Текст кнопки "Подробнее"
            "MESS_BTN_SUBSCRIBE" => "Подписаться",	// Текст кнопки "Уведомить о поступлении"
            "MESS_NOT_AVAILABLE" => "Нет в наличии",	// Сообщение об отсутствии товара
            "META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
            "META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
            "OFFERS_LIMIT" => "5",	// Максимальное количество предложений для показа (0 - все)
            "PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
            "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
            "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
            "PAGER_SHOW_ALWAYS" => "Y",	// Выводить всегда
            "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
            "PAGER_TITLE" => "Товары",	// Название категорий
            "PAGE_ELEMENT_COUNT" => $arParams['PAGE_ELEMENT_COUNT'],	// Количество элементов на странице
            "PARTIAL_PRODUCT_PROPERTIES" => "N",	// Разрешить добавлять в корзину товары, у которых заполнены не все характеристики
            "PRICE_CODE" => $arParams['PRICE_CODE'],	// Тип цены


            // Section
            "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"], // ID раздела
            "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"], // Код раздела
            "SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"], // URL, ведущий на страницу с содержимым раздела
            "DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"], // URL, ведущий на страницу с содержимым элемента раздела
            "USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"], // Использовать основной раздел для показа элемента

            // Error 404
            "MESSAGE_404" => $arParams["~MESSAGE_404"], // Сообщение для показа (по умолчанию из компонента)
            "SET_STATUS_404" => $arParams["SET_STATUS_404"], // Устанавливать статус 404
            "SHOW_404" => $arParams["SHOW_404"], // Показ специальной страницы

            // Basket
            "BASKET_URL" => $arParams["BASKET_URL"], // URL, ведущий на страницу с корзиной покупателя

            "PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
            "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",	// Порядок отображения блоков товара
            "PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
            "PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
            "PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Название переменной, в которой передается количество товара
            "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'3','BIG_DATA':false}]",	// Вариант отображения товаров
            "PRODUCT_SUBSCRIPTION" => "Y",	// Разрешить оповещения для отсутствующих товаров
            "RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],	// Параметр ID продукта (для товарных рекомендаций)
            "RCM_TYPE" => "personal",	// Тип рекомендации

            "SECTION_ID_VARIABLE" => "SECTION_ID",	// Название переменной, в которой передается код группы

            "SECTION_USER_FIELDS" => array(	// Свойства раздела
                0 => "",
                1 => "",
            ),
            "SEF_MODE" => "N",	// Включить поддержку ЧПУ
            "SET_BROWSER_TITLE" => "Y",	// Устанавливать заголовок окна браузера
            "SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
            "SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
            "SET_META_KEYWORDS" => "Y",	// Устанавливать ключевые слова страницы
            "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
            "SHOW_ALL_WO_SECTION" => "N",	// Показывать все элементы, если не указан раздел
            "SHOW_CLOSE_POPUP" => "N",	// Показывать кнопку продолжения покупок во всплывающих окнах
            "SHOW_DISCOUNT_PERCENT" => "N",	// Показывать процент скидки
            "SHOW_FROM_SECTION" => "Y",	// Показывать товары из раздела
            "SHOW_MAX_QUANTITY" => "N",	// Показывать остаток товара
            "SHOW_OLD_PRICE" => "N",	// Показывать старую цену
            "SHOW_PRICE_COUNT" => $arParams['SHOW_PRICE_COUNT'],	// Выводить цены для количества
            "SHOW_SLIDER" => "N",	// Показывать слайдер для товаров
            "SLIDER_INTERVAL" => "3000",
            "SLIDER_PROGRESS" => "N",
            "TEMPLATE_THEME" => "blue",	// Цветовая тема
            "USE_ENHANCED_ECOMMERCE" => "N",	// Отправлять данные электронной торговли в Google и Яндекс
            "USE_PRICE_COUNT" => "N",	// Использовать вывод цен с диапазонами
            "USE_PRODUCT_QUANTITY" => "N",	// Разрешить указание количества товара
        ),
            false
        );?>
    </div>
</div>
