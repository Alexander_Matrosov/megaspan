<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
$this->setFrameMode(true);
?>
<div class="container">
    <div class="catalog-item">
        <div class="breadcrumbs"><a href="#">Главная страница</a><a href="#">Мегаспан ГЕО 100</a></div>
        <h1 class="catalog__title">Мегаспан ГЕО 100</h1>
        <?$APPLICATION->IncludeComponent("bitrix:catalog.element", "main.element", Array(
            "ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
            "ADD_DETAIL_TO_SLIDER" => "N",	// Добавлять детальную картинку в слайдер
            "ADD_ELEMENT_CHAIN" => "N",	// Включать название элемента в цепочку навигации
            "ADD_PICT_PROP" => "-",	// Дополнительная картинка основного товара
            "ADD_PROPERTIES_TO_BASKET" => $arParams['ADD_PROPERTIES_TO_BASKET'],	// Добавлять в корзину свойства товаров и предложений
            "ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
            "ADD_TO_BASKET_ACTION" => $arParams['ADD_TO_BASKET_ACTION'],	// Показывать кнопки добавления в корзину и покупки
            "ADD_TO_BASKET_ACTION_PRIMARY" => $arParams['ADD_TO_BASKET_ACTION_PRIMARY'],	// Выделять кнопки добавления в корзину и покупки
            "BACKGROUND_IMAGE" => "-",	// Установить фоновую картинку для шаблона из свойства
            "BASKET_URL" => $arParams['BASKET_URL'],	// URL, ведущий на страницу с корзиной покупателя
            "BRAND_USE" => "N",	// Использовать компонент "Бренды"
            "BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
            "CACHE_GROUPS" => $arParams['CACHE_GROUPS'],	// Учитывать права доступа
            "CACHE_TIME" => $arParams['CACHE_TIME'],	// Время кеширования (сек.)
            "CACHE_TYPE" => $arParams['CACHE_TYPE'],	// Тип кеширования
            "CHECK_SECTION_ID_VARIABLE" => "N",	// Использовать код группы из переменной, если не задан раздел элемента
            "COMPATIBLE_MODE" => "Y",	// Включить режим совместимости
            "CONVERT_CURRENCY" => "N",	// Показывать цены в одной валюте
            "DETAIL_PICTURE_MODE" => array(	// Режим показа детальной картинки
                0 => "POPUP",
                1 => "MAGNIFIER",
            ),
            "DISABLE_INIT_JS_IN_COMPONENT" => "N",	// Не подключать js-библиотеки в компоненте
            "DISPLAY_COMPARE" => "N",	// Разрешить сравнение товаров
            "DISPLAY_NAME" => "Y",	// Выводить название элемента
            "DISPLAY_PREVIEW_TEXT_MODE" => "E",	// Показ описания для анонса

            "SECTION_CODE" => $arResult['VARIABLES']['SECTION_CODE'],	// Код раздела
            "SECTION_ID" => $arResult['VARIABLES']['SECTION_ID'],	// ID раздела
            'SECTION_URL' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['section'],	// URL, ведущий на страницу с содержимым раздела
            "ELEMENT_CODE" => $arResult['VARIABLES']['ELEMENT_CODE'],	// Код элемента
            "ELEMENT_ID" => $arResult['VARIABLES']['ELEMENT_ID'],	// ID элемента
            'DETAIL_URL' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['element'],	// URL, ведущий на страницу с содержимым элемента раздела

            "GIFTS_DETAIL_BLOCK_TITLE" => "Выберите один из подарков",	// Текст заголовка "Подарки"
            "GIFTS_DETAIL_HIDE_BLOCK_TITLE" => "N",	// Скрыть заголовок "Подарки" в детальном просмотре
            "GIFTS_DETAIL_PAGE_ELEMENT_COUNT" => "4",	// Количество элементов в блоке "Подарки" в строке в детальном просмотре
            "GIFTS_DETAIL_TEXT_LABEL_GIFT" => "Подарок",	// Текст метки "Подарка" в детальном просмотре
            "GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE" => "Выберите один из товаров, чтобы получить подарок",	// Текст заголовка "Товары к подарку"
            "GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE" => "N",	// Скрыть заголовок "Товары к подарку" в детальном просмотре
            "GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT" => "4",	// Количество элементов в блоке "Товары к подарку" в строке в детальном просмотре
            "GIFTS_MESS_BTN_BUY" => "Выбрать",	// Текст кнопки "Выбрать"
            "GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",	// Показывать процент скидки
            "GIFTS_SHOW_IMAGE" => "Y",	// Показывать изображение
            "GIFTS_SHOW_NAME" => "Y",	// Показывать название
            "GIFTS_SHOW_OLD_PRICE" => "Y",	// Показывать старую цену
            "HIDE_NOT_AVAILABLE_OFFERS" => "N",	// Недоступные торговые предложения
            "IBLOCK_ID" => $arParams['IBLOCK_ID'],	// Инфоблок
            "IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],	// Тип инфоблока
            "IMAGE_RESOLUTION" => "16by9",	// Соотношение сторон изображения товара
            "LABEL_PROP" => "",	// Свойство меток товара
            "LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",	// URL на страницу, где будет показан список связанных элементов
            "LINK_IBLOCK_ID" => "",	// ID инфоблока, элементы которого связаны с текущим элементом
            "LINK_IBLOCK_TYPE" => "",	// Тип инфоблока, элементы которого связаны с текущим элементом
            "LINK_PROPERTY_SID" => "",	// Свойство, в котором хранится связь
            "MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
            "MESS_BTN_ADD_TO_BASKET" => "В корзину",	// Текст кнопки "Добавить в корзину"
            "MESS_BTN_BUY" => "Купить",	// Текст кнопки "Купить"
            "MESS_BTN_SUBSCRIBE" => "Подписаться",	// Текст кнопки "Уведомить о поступлении"
            "MESS_COMMENTS_TAB" => "Комментарии",	// Текст вкладки "Комментарии"
            "MESS_DESCRIPTION_TAB" => "Описание",	// Текст вкладки "Описание"
            "MESS_NOT_AVAILABLE" => "Нет в наличии",	// Сообщение об отсутствии товара
            "MESS_PRICE_RANGES_TITLE" => "Цены",	// Название блока c расширенными ценами
            "MESS_PROPERTIES_TAB" => "Характеристики",	// Текст вкладки "Характеристики"
            "META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
            "META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
            "OFFERS_LIMIT" => "0",	// Максимальное количество предложений для показа (0 - все)
            "PARTIAL_PRODUCT_PROPERTIES" => $arParams['PARTIAL_PRODUCT_PROPERTIES'],	// Разрешить добавлять в корзину товары, у которых заполнены не все характеристики
            "PRICE_CODE" => $arParams['PRICE_CODE'], // Тип цены
            "PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
            "PRICE_VAT_SHOW_VALUE" => "N",	// Отображать значение НДС
            "PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
            "PRODUCT_INFO_BLOCK_ORDER" => "sku,props",	// Порядок отображения блоков информации о товаре
            "PRODUCT_PAY_BLOCK_ORDER" => "rating,price,priceRanges,quantityLimit,quantity,buttons",	// Порядок отображения блоков покупки товара
            "PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
            "PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Название переменной, в которой передается количество товара
            "PRODUCT_SUBSCRIPTION" => "Y",	// Разрешить оповещения для отсутствующих товаров
            "SECTION_ID_VARIABLE" => "SECTION_ID",	// Название переменной, в которой передается код группы
            "SEF_MODE" => "N",	// Включить поддержку ЧПУ
            "SET_BROWSER_TITLE" => "Y",	// Устанавливать заголовок окна браузера
            "SET_CANONICAL_URL" => "N",	// Устанавливать канонический URL
            "SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
            "SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
            "SET_META_KEYWORDS" => "Y",	// Устанавливать ключевые слова страницы
            "SET_STATUS_404" => "N",	// Устанавливать статус 404
            "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
            "SET_VIEWED_IN_COMPONENT" => "N",	// Включить сохранение информации о просмотре товара для старых шаблонов
            "SHOW_404" => "N",	// Показ специальной страницы
            "SHOW_CLOSE_POPUP" => "N",	// Показывать кнопку продолжения покупок во всплывающих окнах
            "SHOW_DEACTIVATED" => "N",	// Показывать деактивированные товары
            "SHOW_DISCOUNT_PERCENT" => "N",	// Показывать процент скидки
            "SHOW_MAX_QUANTITY" => "N",	// Показывать остаток товара
            "SHOW_OLD_PRICE" => "N",	// Показывать старую цену
            "SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества
            "SHOW_SLIDER" => "N",	// Показывать слайдер для товаров
            "STRICT_SECTION_CHECK" => "N",	// Строгая проверка раздела для показа элемента
            "TEMPLATE_THEME" => "blue",	// Цветовая тема
            "USE_COMMENTS" => "N",	// Включить отзывы о товаре
            "USE_ELEMENT_COUNTER" => "Y",	// Использовать счетчик просмотров
            "USE_ENHANCED_ECOMMERCE" => "N",	// Включить отправку данных в электронную торговлю
            "USE_GIFTS_DETAIL" => "Y",	// Показывать блок "Подарки" в детальном просмотре
            "USE_GIFTS_MAIN_PR_SECTION_LIST" => "Y",	// Показывать блок "Товары к подарку" в детальном просмотре
            "USE_MAIN_ELEMENT_SECTION" => "N",	// Использовать основной раздел для показа элемента
            "USE_PRICE_COUNT" => "N",	// Использовать вывод цен с диапазонами
            "USE_PRODUCT_QUANTITY" => "N",	// Разрешить указание количества товара
            "USE_RATIO_IN_RANGES" => "N",	// Учитывать коэффициенты для диапазонов цен
            "USE_VOTE_RATING" => "N",	// Включить рейтинг товара
        ),
            false
        );?>
        <div class="wrapper-card"><span class="wrapper-card__title">Возможно понадобится:</span>
            <div class="card-list">
                <div class="card">
                    <div class="card-img"><img src="./img/card-img.jpg" alt=""></div>
                    <div class="card-name"><a class="stock stock-list" href="#">«Держи пять»</a>
                        <p>МЕГАСПАН ГЕО 100 - 40м2 (1,6м*25п.м.) под тротуарную плитку</p>
                        <div class="card-box">
                            <p>цена</p><span>м2</span><a class="stock stock-row" href="#">«Держи пять»</a>
                        </div>
                    </div><a class="in-favorite__row" href="#"><svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M7.00023 7.16667C4.20982 8.841 2.41689 12.1667 12.0001 19.6667C21.1666 12.1667 19.4999 9.25 17.8333 7.58334C15.8987 5.64883 13.2501 7.16667 12.0001 8.41667C10.7501 7.16667 9.08346 5.91667 7.00023 7.16667Z" stroke="#44A63D" stroke-width="2.5"/>
                        </svg></a>
                    <div class="card-info">
                        <div class="card-box">
                            <p>цена</p><span>м2</span>
                        </div>
                        <div class="card-price">
                            <p> <span>13256,53</span><span>руб.</span></p>
                            <p>по 5 шт.</p>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="card-count"><span>
                       <svg width="8" height="2" viewBox="0 0 8 2" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect width="8" height="2" fill="#CCCCCC"/>
</svg></span>
                            <input type="text" value="0"><span>
                       <svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M5 3V0H3V3H0V5H3V8H5V5H8V3H5Z" fill="#44A63D"/>
</svg></span>
                        </div><a class="btn" href="#"><svg width="64" height="35" viewBox="0 0 64 35" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <mask id="path-1-inside-1" fill="white">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M0 35V10L10 0H10.9714H32H64V25L54 35H51.2H0Z"/>
                                </mask>
                                <path class="btn-background" fill-rule="evenodd" clip-rule="evenodd" d="M0 35V10L10 0H10.9714H32H64V25L54 35H51.2H0Z" fill="white"/>
                                <path d="M0 10L-0.707107 9.29289L-1 9.58579V10H0ZM0 35H-1V36H0V35ZM10 0V-1H9.58579L9.29289 -0.707107L10 0ZM64 0H65V-1H64V0ZM64 25L64.7071 25.7071L65 25.4142V25H64ZM54 35V36H54.4142L54.7071 35.7071L54 35ZM-1 10V35H1V10H-1ZM9.29289 -0.707107L-0.707107 9.29289L0.707107 10.7071L10.7071 0.707107L9.29289 -0.707107ZM10.9714 -1H10V1H10.9714V-1ZM32 -1H10.9714V1H32V-1ZM64 -1H32V1H64V-1ZM65 25V0H63V25H65ZM54.7071 35.7071L64.7071 25.7071L63.2929 24.2929L53.2929 34.2929L54.7071 35.7071ZM51.2 36H54V34H51.2V36ZM0 36H51.2V34H0V36Z" fill="#44A63D" mask="url(#path-1-inside-1)"/>
                                <path class="btn-logo" d="M29.4939 24.9445C29.8714 24.9445 30.1775 24.6336 30.1775 24.25C30.1775 23.8665 29.8714 23.5556 29.4939 23.5556C29.1164 23.5556 28.8103 23.8665 28.8103 24.25C28.8103 24.6336 29.1164 24.9445 29.4939 24.9445Z" stroke="#44A63D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                <path class="btn-logo" d="M37.0134 24.9445C37.391 24.9445 37.697 24.6336 37.697 24.25C37.697 23.8665 37.391 23.5556 37.0134 23.5556C36.6359 23.5556 36.3298 23.8665 36.3298 24.25C36.3298 24.6336 36.6359 24.9445 37.0134 24.9445Z" stroke="#44A63D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                <path class="btn-logo" d="M24.0251 10.3611H26.7595L28.5916 19.6598C28.6541 19.9795 28.8253 20.2667 29.0752 20.4711C29.3252 20.6755 29.6379 20.7841 29.9587 20.7778H36.6033C36.9241 20.7841 37.2368 20.6755 37.4868 20.4711C37.7367 20.2667 37.9079 19.9795 37.9705 19.6598L39.0642 13.8334H27.4431" stroke="#44A63D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg></a>
                    </div><a class="in-favorite" href="#"><svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M7.00023 7.16667C4.20982 8.841 2.41689 12.1667 12.0001 19.6667C21.1666 12.1667 19.4999 9.25 17.8333 7.58334C15.8987 5.64883 13.2501 7.16667 12.0001 8.41667C10.7501 7.16667 9.08346 5.91667 7.00023 7.16667Z" stroke="#44A63D" stroke-width="2.5"/>
                        </svg><span>в «Мой ассортимент»</span></a>
                </div>
                <div class="card">
                    <div class="card-img"><img src="./img/card-img.jpg" alt=""></div>
                    <div class="card-name"><a class="stock stock-list" href="#">«Держи пять»</a>
                        <p>МЕГАСПАН ГЕО 100 - 40м2 (1,6м*25п.м.) под тротуарную плитку</p>
                        <div class="card-box">
                            <p>цена</p><span>м2</span><a class="stock stock-row" href="#">«Держи пять»</a>
                        </div>
                    </div><a class="in-favorite__row" href="#"><svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M7.00023 7.16667C4.20982 8.841 2.41689 12.1667 12.0001 19.6667C21.1666 12.1667 19.4999 9.25 17.8333 7.58334C15.8987 5.64883 13.2501 7.16667 12.0001 8.41667C10.7501 7.16667 9.08346 5.91667 7.00023 7.16667Z" stroke="#44A63D" stroke-width="2.5"/>
                        </svg></a>
                    <div class="card-info">
                        <div class="card-box">
                            <p>цена</p><span>м2</span>
                        </div>
                        <div class="card-price">
                            <p> <span>13256,53</span><span>руб.</span></p>
                            <p>по 5 шт.</p>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="card-count"><span>
                       <svg width="8" height="2" viewBox="0 0 8 2" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect width="8" height="2" fill="#CCCCCC"/>
</svg></span>
                            <input type="text" value="0"><span>
                       <svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M5 3V0H3V3H0V5H3V8H5V5H8V3H5Z" fill="#44A63D"/>
</svg></span>
                        </div><a class="btn" href="#"><svg width="64" height="35" viewBox="0 0 64 35" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <mask id="path-1-inside-1" fill="white">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M0 35V10L10 0H10.9714H32H64V25L54 35H51.2H0Z"/>
                                </mask>
                                <path class="btn-background" fill-rule="evenodd" clip-rule="evenodd" d="M0 35V10L10 0H10.9714H32H64V25L54 35H51.2H0Z" fill="white"/>
                                <path d="M0 10L-0.707107 9.29289L-1 9.58579V10H0ZM0 35H-1V36H0V35ZM10 0V-1H9.58579L9.29289 -0.707107L10 0ZM64 0H65V-1H64V0ZM64 25L64.7071 25.7071L65 25.4142V25H64ZM54 35V36H54.4142L54.7071 35.7071L54 35ZM-1 10V35H1V10H-1ZM9.29289 -0.707107L-0.707107 9.29289L0.707107 10.7071L10.7071 0.707107L9.29289 -0.707107ZM10.9714 -1H10V1H10.9714V-1ZM32 -1H10.9714V1H32V-1ZM64 -1H32V1H64V-1ZM65 25V0H63V25H65ZM54.7071 35.7071L64.7071 25.7071L63.2929 24.2929L53.2929 34.2929L54.7071 35.7071ZM51.2 36H54V34H51.2V36ZM0 36H51.2V34H0V36Z" fill="#44A63D" mask="url(#path-1-inside-1)"/>
                                <path class="btn-logo" d="M29.4939 24.9445C29.8714 24.9445 30.1775 24.6336 30.1775 24.25C30.1775 23.8665 29.8714 23.5556 29.4939 23.5556C29.1164 23.5556 28.8103 23.8665 28.8103 24.25C28.8103 24.6336 29.1164 24.9445 29.4939 24.9445Z" stroke="#44A63D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                <path class="btn-logo" d="M37.0134 24.9445C37.391 24.9445 37.697 24.6336 37.697 24.25C37.697 23.8665 37.391 23.5556 37.0134 23.5556C36.6359 23.5556 36.3298 23.8665 36.3298 24.25C36.3298 24.6336 36.6359 24.9445 37.0134 24.9445Z" stroke="#44A63D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                <path class="btn-logo" d="M24.0251 10.3611H26.7595L28.5916 19.6598C28.6541 19.9795 28.8253 20.2667 29.0752 20.4711C29.3252 20.6755 29.6379 20.7841 29.9587 20.7778H36.6033C36.9241 20.7841 37.2368 20.6755 37.4868 20.4711C37.7367 20.2667 37.9079 19.9795 37.9705 19.6598L39.0642 13.8334H27.4431" stroke="#44A63D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg></a>
                    </div><a class="in-favorite" href="#"><svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M7.00023 7.16667C4.20982 8.841 2.41689 12.1667 12.0001 19.6667C21.1666 12.1667 19.4999 9.25 17.8333 7.58334C15.8987 5.64883 13.2501 7.16667 12.0001 8.41667C10.7501 7.16667 9.08346 5.91667 7.00023 7.16667Z" stroke="#44A63D" stroke-width="2.5"/>
                        </svg><span>в «Мой ассортимент»</span></a>
                </div>
                <div class="card">
                    <div class="card-img"><img src="./img/card-img.jpg" alt=""></div>
                    <div class="card-name"><a class="stock stock-list" href="#">«Держи пять»</a>
                        <p>МЕГАСПАН ГЕО 100 - 40м2 (1,6м*25п.м.) под тротуарную плитку</p>
                        <div class="card-box">
                            <p>цена</p><span>м2</span><a class="stock stock-row" href="#">«Держи пять»</a>
                        </div>
                    </div><a class="in-favorite__row" href="#"><svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M7.00023 7.16667C4.20982 8.841 2.41689 12.1667 12.0001 19.6667C21.1666 12.1667 19.4999 9.25 17.8333 7.58334C15.8987 5.64883 13.2501 7.16667 12.0001 8.41667C10.7501 7.16667 9.08346 5.91667 7.00023 7.16667Z" stroke="#44A63D" stroke-width="2.5"/>
                        </svg></a>
                    <div class="card-info">
                        <div class="card-box">
                            <p>цена</p><span>м2</span>
                        </div>
                        <div class="card-price">
                            <p> <span>13256,53</span><span>руб.</span></p>
                            <p>по 5 шт.</p>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="card-count"><span>
                       <svg width="8" height="2" viewBox="0 0 8 2" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect width="8" height="2" fill="#CCCCCC"/>
</svg></span>
                            <input type="text" value="0"><span>
                       <svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M5 3V0H3V3H0V5H3V8H5V5H8V3H5Z" fill="#44A63D"/>
</svg></span>
                        </div><a class="btn" href="#"><svg width="64" height="35" viewBox="0 0 64 35" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <mask id="path-1-inside-1" fill="white">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M0 35V10L10 0H10.9714H32H64V25L54 35H51.2H0Z"/>
                                </mask>
                                <path class="btn-background" fill-rule="evenodd" clip-rule="evenodd" d="M0 35V10L10 0H10.9714H32H64V25L54 35H51.2H0Z" fill="white"/>
                                <path d="M0 10L-0.707107 9.29289L-1 9.58579V10H0ZM0 35H-1V36H0V35ZM10 0V-1H9.58579L9.29289 -0.707107L10 0ZM64 0H65V-1H64V0ZM64 25L64.7071 25.7071L65 25.4142V25H64ZM54 35V36H54.4142L54.7071 35.7071L54 35ZM-1 10V35H1V10H-1ZM9.29289 -0.707107L-0.707107 9.29289L0.707107 10.7071L10.7071 0.707107L9.29289 -0.707107ZM10.9714 -1H10V1H10.9714V-1ZM32 -1H10.9714V1H32V-1ZM64 -1H32V1H64V-1ZM65 25V0H63V25H65ZM54.7071 35.7071L64.7071 25.7071L63.2929 24.2929L53.2929 34.2929L54.7071 35.7071ZM51.2 36H54V34H51.2V36ZM0 36H51.2V34H0V36Z" fill="#44A63D" mask="url(#path-1-inside-1)"/>
                                <path class="btn-logo" d="M29.4939 24.9445C29.8714 24.9445 30.1775 24.6336 30.1775 24.25C30.1775 23.8665 29.8714 23.5556 29.4939 23.5556C29.1164 23.5556 28.8103 23.8665 28.8103 24.25C28.8103 24.6336 29.1164 24.9445 29.4939 24.9445Z" stroke="#44A63D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                <path class="btn-logo" d="M37.0134 24.9445C37.391 24.9445 37.697 24.6336 37.697 24.25C37.697 23.8665 37.391 23.5556 37.0134 23.5556C36.6359 23.5556 36.3298 23.8665 36.3298 24.25C36.3298 24.6336 36.6359 24.9445 37.0134 24.9445Z" stroke="#44A63D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                <path class="btn-logo" d="M24.0251 10.3611H26.7595L28.5916 19.6598C28.6541 19.9795 28.8253 20.2667 29.0752 20.4711C29.3252 20.6755 29.6379 20.7841 29.9587 20.7778H36.6033C36.9241 20.7841 37.2368 20.6755 37.4868 20.4711C37.7367 20.2667 37.9079 19.9795 37.9705 19.6598L39.0642 13.8334H27.4431" stroke="#44A63D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg></a>
                    </div><a class="in-favorite" href="#"><svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M7.00023 7.16667C4.20982 8.841 2.41689 12.1667 12.0001 19.6667C21.1666 12.1667 19.4999 9.25 17.8333 7.58334C15.8987 5.64883 13.2501 7.16667 12.0001 8.41667C10.7501 7.16667 9.08346 5.91667 7.00023 7.16667Z" stroke="#44A63D" stroke-width="2.5"/>
                        </svg><span>в «Мой ассортимент»</span></a>
                </div>
                <div class="card">
                    <div class="card-img"><img src="./img/card-img.jpg" alt=""></div>
                    <div class="card-name"><a class="stock stock-list" href="#">«Держи пять»</a>
                        <p>МЕГАСПАН ГЕО 100 - 40м2 (1,6м*25п.м.) под тротуарную плитку</p>
                        <div class="card-box">
                            <p>цена</p><span>м2</span><a class="stock stock-row" href="#">«Держи пять»</a>
                        </div>
                    </div><a class="in-favorite__row" href="#"><svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M7.00023 7.16667C4.20982 8.841 2.41689 12.1667 12.0001 19.6667C21.1666 12.1667 19.4999 9.25 17.8333 7.58334C15.8987 5.64883 13.2501 7.16667 12.0001 8.41667C10.7501 7.16667 9.08346 5.91667 7.00023 7.16667Z" stroke="#44A63D" stroke-width="2.5"/>
                        </svg></a>
                    <div class="card-info">
                        <div class="card-box">
                            <p>цена</p><span>м2</span>
                        </div>
                        <div class="card-price">
                            <p> <span>13256,53</span><span>руб.</span></p>
                            <p>по 5 шт.</p>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="card-count"><span>
                       <svg width="8" height="2" viewBox="0 0 8 2" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect width="8" height="2" fill="#CCCCCC"/>
</svg></span>
                            <input type="text" value="0"><span>
                       <svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M5 3V0H3V3H0V5H3V8H5V5H8V3H5Z" fill="#44A63D"/>
</svg></span>
                        </div><a class="btn" href="#"><svg width="64" height="35" viewBox="0 0 64 35" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <mask id="path-1-inside-1" fill="white">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M0 35V10L10 0H10.9714H32H64V25L54 35H51.2H0Z"/>
                                </mask>
                                <path class="btn-background" fill-rule="evenodd" clip-rule="evenodd" d="M0 35V10L10 0H10.9714H32H64V25L54 35H51.2H0Z" fill="white"/>
                                <path d="M0 10L-0.707107 9.29289L-1 9.58579V10H0ZM0 35H-1V36H0V35ZM10 0V-1H9.58579L9.29289 -0.707107L10 0ZM64 0H65V-1H64V0ZM64 25L64.7071 25.7071L65 25.4142V25H64ZM54 35V36H54.4142L54.7071 35.7071L54 35ZM-1 10V35H1V10H-1ZM9.29289 -0.707107L-0.707107 9.29289L0.707107 10.7071L10.7071 0.707107L9.29289 -0.707107ZM10.9714 -1H10V1H10.9714V-1ZM32 -1H10.9714V1H32V-1ZM64 -1H32V1H64V-1ZM65 25V0H63V25H65ZM54.7071 35.7071L64.7071 25.7071L63.2929 24.2929L53.2929 34.2929L54.7071 35.7071ZM51.2 36H54V34H51.2V36ZM0 36H51.2V34H0V36Z" fill="#44A63D" mask="url(#path-1-inside-1)"/>
                                <path class="btn-logo" d="M29.4939 24.9445C29.8714 24.9445 30.1775 24.6336 30.1775 24.25C30.1775 23.8665 29.8714 23.5556 29.4939 23.5556C29.1164 23.5556 28.8103 23.8665 28.8103 24.25C28.8103 24.6336 29.1164 24.9445 29.4939 24.9445Z" stroke="#44A63D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                <path class="btn-logo" d="M37.0134 24.9445C37.391 24.9445 37.697 24.6336 37.697 24.25C37.697 23.8665 37.391 23.5556 37.0134 23.5556C36.6359 23.5556 36.3298 23.8665 36.3298 24.25C36.3298 24.6336 36.6359 24.9445 37.0134 24.9445Z" stroke="#44A63D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                <path class="btn-logo" d="M24.0251 10.3611H26.7595L28.5916 19.6598C28.6541 19.9795 28.8253 20.2667 29.0752 20.4711C29.3252 20.6755 29.6379 20.7841 29.9587 20.7778H36.6033C36.9241 20.7841 37.2368 20.6755 37.4868 20.4711C37.7367 20.2667 37.9079 19.9795 37.9705 19.6598L39.0642 13.8334H27.4431" stroke="#44A63D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg></a>
                    </div><a class="in-favorite" href="#"><svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M7.00023 7.16667C4.20982 8.841 2.41689 12.1667 12.0001 19.6667C21.1666 12.1667 19.4999 9.25 17.8333 7.58334C15.8987 5.64883 13.2501 7.16667 12.0001 8.41667C10.7501 7.16667 9.08346 5.91667 7.00023 7.16667Z" stroke="#44A63D" stroke-width="2.5"/>
                        </svg><span>в «Мой ассортимент»</span></a>
                </div>
                <div class="card">
                    <div class="card-img"><img src="./img/card-img.jpg" alt=""></div>
                    <div class="card-name"><a class="stock stock-list" href="#">«Держи пять»</a>
                        <p>МЕГАСПАН ГЕО 100 - 40м2 (1,6м*25п.м.) под тротуарную плитку</p>
                        <div class="card-box">
                            <p>цена</p><span>м2</span><a class="stock stock-row" href="#">«Держи пять»</a>
                        </div>
                    </div><a class="in-favorite__row" href="#"><svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M7.00023 7.16667C4.20982 8.841 2.41689 12.1667 12.0001 19.6667C21.1666 12.1667 19.4999 9.25 17.8333 7.58334C15.8987 5.64883 13.2501 7.16667 12.0001 8.41667C10.7501 7.16667 9.08346 5.91667 7.00023 7.16667Z" stroke="#44A63D" stroke-width="2.5"/>
                        </svg></a>
                    <div class="card-info">
                        <div class="card-box">
                            <p>цена</p><span>м2</span>
                        </div>
                        <div class="card-price">
                            <p> <span>13256,53</span><span>руб.</span></p>
                            <p>по 5 шт.</p>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="card-count"><span>
                       <svg width="8" height="2" viewBox="0 0 8 2" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect width="8" height="2" fill="#CCCCCC"/>
</svg></span>
                            <input type="text" value="0"><span>
                       <svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M5 3V0H3V3H0V5H3V8H5V5H8V3H5Z" fill="#44A63D"/>
</svg></span>
                        </div><a class="btn" href="#"><svg width="64" height="35" viewBox="0 0 64 35" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <mask id="path-1-inside-1" fill="white">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M0 35V10L10 0H10.9714H32H64V25L54 35H51.2H0Z"/>
                                </mask>
                                <path class="btn-background" fill-rule="evenodd" clip-rule="evenodd" d="M0 35V10L10 0H10.9714H32H64V25L54 35H51.2H0Z" fill="white"/>
                                <path d="M0 10L-0.707107 9.29289L-1 9.58579V10H0ZM0 35H-1V36H0V35ZM10 0V-1H9.58579L9.29289 -0.707107L10 0ZM64 0H65V-1H64V0ZM64 25L64.7071 25.7071L65 25.4142V25H64ZM54 35V36H54.4142L54.7071 35.7071L54 35ZM-1 10V35H1V10H-1ZM9.29289 -0.707107L-0.707107 9.29289L0.707107 10.7071L10.7071 0.707107L9.29289 -0.707107ZM10.9714 -1H10V1H10.9714V-1ZM32 -1H10.9714V1H32V-1ZM64 -1H32V1H64V-1ZM65 25V0H63V25H65ZM54.7071 35.7071L64.7071 25.7071L63.2929 24.2929L53.2929 34.2929L54.7071 35.7071ZM51.2 36H54V34H51.2V36ZM0 36H51.2V34H0V36Z" fill="#44A63D" mask="url(#path-1-inside-1)"/>
                                <path class="btn-logo" d="M29.4939 24.9445C29.8714 24.9445 30.1775 24.6336 30.1775 24.25C30.1775 23.8665 29.8714 23.5556 29.4939 23.5556C29.1164 23.5556 28.8103 23.8665 28.8103 24.25C28.8103 24.6336 29.1164 24.9445 29.4939 24.9445Z" stroke="#44A63D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                <path class="btn-logo" d="M37.0134 24.9445C37.391 24.9445 37.697 24.6336 37.697 24.25C37.697 23.8665 37.391 23.5556 37.0134 23.5556C36.6359 23.5556 36.3298 23.8665 36.3298 24.25C36.3298 24.6336 36.6359 24.9445 37.0134 24.9445Z" stroke="#44A63D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                <path class="btn-logo" d="M24.0251 10.3611H26.7595L28.5916 19.6598C28.6541 19.9795 28.8253 20.2667 29.0752 20.4711C29.3252 20.6755 29.6379 20.7841 29.9587 20.7778H36.6033C36.9241 20.7841 37.2368 20.6755 37.4868 20.4711C37.7367 20.2667 37.9079 19.9795 37.9705 19.6598L39.0642 13.8334H27.4431" stroke="#44A63D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg></a>
                    </div><a class="in-favorite" href="#"><svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M7.00023 7.16667C4.20982 8.841 2.41689 12.1667 12.0001 19.6667C21.1666 12.1667 19.4999 9.25 17.8333 7.58334C15.8987 5.64883 13.2501 7.16667 12.0001 8.41667C10.7501 7.16667 9.08346 5.91667 7.00023 7.16667Z" stroke="#44A63D" stroke-width="2.5"/>
                        </svg><span>в «Мой ассортимент»</span></a>
                </div>
            </div>
        </div>
    </div>
</div>

