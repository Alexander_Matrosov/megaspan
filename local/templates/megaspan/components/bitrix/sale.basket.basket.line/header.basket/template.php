<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/**
 * @global string $componentPath
 * @global string $templateName
 * @var CBitrixComponentTemplate $this
 */

?>
<a href="/personal/cart/">
    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M7.4939 16.9444C7.87143 16.9444 8.17749 16.6335 8.17749 16.25C8.17749 15.8665 7.87143 15.5555 7.4939 15.5555C7.11636 15.5555 6.8103 15.8665 6.8103 16.25C6.8103 16.6335 7.11636 16.9444 7.4939 16.9444Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M15.0134 16.9444C15.391 16.9444 15.697 16.6335 15.697 16.25C15.697 15.8665 15.391 15.5555 15.0134 15.5555C14.6359 15.5555 14.3298 15.8665 14.3298 16.25C14.3298 16.6335 14.6359 16.9444 15.0134 16.9444Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M2.02515 2.36108H4.75952L6.59155 11.6597C6.65406 11.9794 6.82528 12.2666 7.07523 12.471C7.32517 12.6754 7.63793 12.784 7.95874 12.7778H14.6033C14.9241 12.784 15.2368 12.6754 15.4868 12.471C15.7367 12.2666 15.9079 11.9794 15.9705 11.6597L17.0642 5.83331H5.44311" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
    </svg>
    <p>Корзина</p>

    <span><?= $arResult['NUM_PRODUCTS']?></span>
</a>